module.exports = {
  projects: [
    '<rootDir>/apps/fifa',
    '<rootDir>/libs/ui-common',
    '<rootDir>/libs/feature-home',
    '<rootDir>/libs/feature-table',
    '<rootDir>/libs/feature-tournaments',
    '<rootDir>/libs/feature-credits',
    '<rootDir>/libs/util-common',
    '<rootDir>/apps/player-administration-backend',
    '<rootDir>/apps/player-administration-frontend',
    '<rootDir>/libs/feature-player-administration-input'
  ]
};
