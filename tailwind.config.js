module.exports = {
  purge: {
    content: ['./apps/**/*.{html,ts,scss}', './libs/**/*.{html,ts,scss}']
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    textColor: (theme) => ({
      ...theme('colors'),
      infoBanner: '#ffffff'
    }),
    backgroundColor: (theme) => ({
      ...theme('colors'),
      primary: '#e8e8e8',
      secondary: '#495464',
      accent: '#2E2E2E'
    }),
    minHeight: {
      testimonial: '10rem'
    },
    borderColor: (theme) => ({
      ...theme('colors'),
      accent: '#2E2E2E',
      primary: '#e8e8e8',
      secondary: '#495464'
    }),

    extend: {}
  },
  variants: {
    extend: {
      backgroundColor: ['even']
    }
  },
  plugins: []
};
