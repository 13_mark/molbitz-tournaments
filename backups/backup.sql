--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE molbitz;




--
-- Drop roles
--

DROP ROLE mark_database_admin;


--
-- Roles
--

CREATE ROLE mark_database_admin;
ALTER ROLE mark_database_admin WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md570c20432a14f2399df065b5578f353ad';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: mark_database_admin
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO mark_database_admin;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: mark_database_admin
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: mark_database_admin
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: mark_database_admin
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "molbitz" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: molbitz; Type: DATABASE; Schema: -; Owner: mark_database_admin
--

CREATE DATABASE molbitz WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE molbitz OWNER TO mark_database_admin;

\connect molbitz

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: additional_information; Type: TABLE; Schema: public; Owner: mark_database_admin
--

CREATE TABLE public.additional_information (
    id integer NOT NULL,
    "extraTimeHomePlayerScore" integer NOT NULL,
    "extraTimeAwayPlayerScore" integer NOT NULL,
    "penaltyShootoutHomePlayerScore" integer,
    "penaltyShootoutAwayPlayerScore" integer
);


ALTER TABLE public.additional_information OWNER TO mark_database_admin;

--
-- Name: additional_information_id_seq; Type: SEQUENCE; Schema: public; Owner: mark_database_admin
--

CREATE SEQUENCE public.additional_information_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.additional_information_id_seq OWNER TO mark_database_admin;

--
-- Name: additional_information_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mark_database_admin
--

ALTER SEQUENCE public.additional_information_id_seq OWNED BY public.additional_information.id;


--
-- Name: game; Type: TABLE; Schema: public; Owner: mark_database_admin
--

CREATE TABLE public.game (
    id integer NOT NULL,
    "homePlayerScore" integer NOT NULL,
    "awayPlayerScore" integer NOT NULL,
    "roundId" integer,
    "homePlayerId" integer,
    "awayPlayerId" integer,
    "additionalInformationId" integer
);


ALTER TABLE public.game OWNER TO mark_database_admin;

--
-- Name: game_id_seq; Type: SEQUENCE; Schema: public; Owner: mark_database_admin
--

CREATE SEQUENCE public.game_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.game_id_seq OWNER TO mark_database_admin;

--
-- Name: game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mark_database_admin
--

ALTER SEQUENCE public.game_id_seq OWNED BY public.game.id;


--
-- Name: player; Type: TABLE; Schema: public; Owner: mark_database_admin
--

CREATE TABLE public.player (
    id integer NOT NULL,
    name character varying NOT NULL,
    points integer DEFAULT 0,
    "goalsScored" integer DEFAULT 0,
    "goalsConceded" integer DEFAULT 0,
    wins integer DEFAULT 0,
    draws integer DEFAULT 0,
    loses integer DEFAULT 0,
    titles integer DEFAULT 0,
    "highestWinId" integer
);


ALTER TABLE public.player OWNER TO mark_database_admin;

--
-- Name: player_id_seq; Type: SEQUENCE; Schema: public; Owner: mark_database_admin
--

CREATE SEQUENCE public.player_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.player_id_seq OWNER TO mark_database_admin;

--
-- Name: player_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mark_database_admin
--

ALTER SEQUENCE public.player_id_seq OWNED BY public.player.id;


--
-- Name: round; Type: TABLE; Schema: public; Owner: mark_database_admin
--

CREATE TABLE public.round (
    id integer NOT NULL,
    title character varying NOT NULL,
    "tournamentId" integer
);


ALTER TABLE public.round OWNER TO mark_database_admin;

--
-- Name: round_id_seq; Type: SEQUENCE; Schema: public; Owner: mark_database_admin
--

CREATE SEQUENCE public.round_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.round_id_seq OWNER TO mark_database_admin;

--
-- Name: round_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mark_database_admin
--

ALTER SEQUENCE public.round_id_seq OWNED BY public.round.id;


--
-- Name: tournament; Type: TABLE; Schema: public; Owner: mark_database_admin
--

CREATE TABLE public.tournament (
    id integer NOT NULL,
    date date NOT NULL
);


ALTER TABLE public.tournament OWNER TO mark_database_admin;

--
-- Name: tournament_id_seq; Type: SEQUENCE; Schema: public; Owner: mark_database_admin
--

CREATE SEQUENCE public.tournament_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tournament_id_seq OWNER TO mark_database_admin;

--
-- Name: tournament_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mark_database_admin
--

ALTER SEQUENCE public.tournament_id_seq OWNED BY public.tournament.id;


--
-- Name: additional_information id; Type: DEFAULT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.additional_information ALTER COLUMN id SET DEFAULT nextval('public.additional_information_id_seq'::regclass);


--
-- Name: game id; Type: DEFAULT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game ALTER COLUMN id SET DEFAULT nextval('public.game_id_seq'::regclass);


--
-- Name: player id; Type: DEFAULT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.player ALTER COLUMN id SET DEFAULT nextval('public.player_id_seq'::regclass);


--
-- Name: round id; Type: DEFAULT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.round ALTER COLUMN id SET DEFAULT nextval('public.round_id_seq'::regclass);


--
-- Name: tournament id; Type: DEFAULT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.tournament ALTER COLUMN id SET DEFAULT nextval('public.tournament_id_seq'::regclass);


--
-- Data for Name: additional_information; Type: TABLE DATA; Schema: public; Owner: mark_database_admin
--

COPY public.additional_information (id, "extraTimeHomePlayerScore", "extraTimeAwayPlayerScore", "penaltyShootoutHomePlayerScore", "penaltyShootoutAwayPlayerScore") FROM stdin;
1	1	1	2	1
2	1	1	1	3
3	1	1	2	1
4	2	2	4	2
5	1	1	2	1
6	1	1	3	1
7	0	0	0	2
8	1	1	1	3
9	1	1	4	3
10	2	0	\N	\N
11	2	2	5	3
12	3	1	\N	\N
13	1	1	2	1
14	2	2	4	2
15	3	2	\N	\N
\.


--
-- Data for Name: game; Type: TABLE DATA; Schema: public; Owner: mark_database_admin
--

COPY public.game (id, "homePlayerScore", "awayPlayerScore", "roundId", "homePlayerId", "awayPlayerId", "additionalInformationId") FROM stdin;
1	0	1	1	1	2	\N
2	1	0	1	5	7	\N
3	4	0	1	9	17	\N
4	0	3	1	4	11	\N
5	0	1	1	8	6	\N
6	0	3	2	7	3	\N
7	0	2	2	2	5	\N
8	0	3	2	17	4	\N
9	1	0	2	6	9	\N
10	1	0	2	11	8	\N
11	2	2	3	5	1	\N
12	3	0	3	3	2	\N
13	1	0	3	6	17	\N
14	2	2	3	8	4	\N
15	1	2	3	9	11	\N
16	1	2	4	2	7	\N
17	0	7	4	1	3	\N
18	0	4	4	17	8	\N
19	1	0	4	11	6	\N
20	0	2	4	4	9	\N
21	2	2	5	3	5	\N
22	1	2	5	7	1	\N
23	7	0	5	11	17	\N
24	5	1	5	9	8	\N
25	0	1	5	6	4	\N
26	1	0	6	3	4	\N
27	0	2	6	1	6	\N
28	3	1	6	11	7	\N
29	3	0	6	9	5	\N
30	2	0	7	3	6	\N
31	2	3	7	11	9	\N
32	1	0	8	3	9	\N
33	2	3	9	6	11	\N
34	6	1	10	3	11	\N
35	0	0	10	9	7	\N
36	1	0	10	8	44	\N
37	0	1	10	17	4	\N
38	0	8	11	45	3	\N
39	0	0	11	6	9	\N
40	0	2	11	44	5	\N
41	5	0	12	11	45	\N
42	1	1	12	7	6	\N
43	1	1	12	5	8	\N
44	0	2	13	6	3	\N
45	1	1	13	11	7	1
46	0	3	13	17	5	\N
47	0	6	13	8	4	\N
48	5	1	14	3	5	\N
49	2	0	14	4	11	\N
50	2	0	15	3	4	\N
51	0	1	16	9	7	\N
52	0	0	16	1	8	\N
53	0	1	16	2	3	\N
54	0	2	16	6	4	\N
55	1	1	17	11	9	\N
56	3	2	17	17	1	\N
57	0	1	17	44	2	\N
58	2	0	17	5	6	\N
59	3	3	18	7	11	\N
60	0	0	18	8	17	\N
61	2	0	18	3	44	\N
62	0	2	18	4	5	\N
63	2	0	19	4	44	\N
64	1	1	19	2	6	2
65	1	1	19	11	1	3
66	2	2	19	8	9	4
67	1	1	20	5	6	5
68	4	1	20	3	11	\N
69	1	1	20	17	8	6
70	0	1	20	7	4	\N
71	0	2	21	44	6	\N
72	2	1	21	2	11	\N
73	0	0	21	1	8	7
74	2	0	21	9	7	\N
75	0	1	22	5	3	\N
76	1	1	22	17	4	8
77	0	2	23	6	5	\N
78	1	2	23	8	2	\N
79	3	0	23	9	17	\N
80	1	0	24	5	2	\N
81	1	2	24	9	8	\N
82	1	0	25	8	5	\N
83	0	2	26	4	3	\N
84	1	0	27	8	4	\N
85	1	4	28	8	3	\N
86	0	1	29	33	17	\N
87	2	1	29	5	3	\N
88	2	0	29	4	8	\N
89	1	5	29	9	7	\N
90	0	0	29	14	11	\N
91	0	1	30	14	33	\N
92	2	1	30	6	5	\N
93	0	1	30	17	4	\N
94	0	2	30	3	9	\N
95	3	0	30	11	8	\N
96	0	3	31	33	4	\N
97	1	0	31	5	9	\N
98	0	2	31	14	8	\N
99	0	1	31	6	7	\N
100	3	0	31	17	11	\N
101	0	2	32	33	8	\N
102	1	3	32	5	7	\N
103	0	2	32	17	14	\N
104	1	0	32	3	6	\N
105	0	3	32	11	4	\N
106	0	0	33	8	17	\N
107	0	3	33	7	3	\N
108	0	1	33	14	4	\N
109	0	0	33	6	9	\N
110	0	1	33	33	11	\N
111	0	0	34	17	33	\N
112	1	1	34	3	5	\N
113	0	4	34	8	4	\N
114	4	1	34	7	9	\N
115	0	1	34	11	14	\N
116	1	2	35	33	14	\N
117	1	2	35	5	6	\N
118	3	1	35	4	17	\N
119	1	0	35	9	3	\N
120	0	2	35	8	11	\N
121	4	3	36	4	33	\N
122	0	3	36	9	5	\N
123	1	1	36	8	14	\N
124	2	0	36	7	6	\N
125	0	1	36	11	17	\N
126	0	1	37	8	33	\N
127	0	1	37	7	5	\N
128	0	2	37	6	3	\N
129	1	3	37	4	11	\N
130	1	1	38	17	8	\N
131	0	0	38	3	7	\N
132	3	1	38	4	14	\N
133	0	0	38	9	6	\N
134	3	1	38	11	33	\N
135	2	0	39	6	4	\N
136	2	3	39	5	17	\N
137	1	2	39	14	7	\N
138	0	1	39	11	3	\N
139	1	0	40	6	17	\N
140	1	1	40	3	7	9
141	2	3	41	6	3	\N
142	0	0	42	11	17	\N
143	1	0	42	12	33	\N
144	0	3	42	6	16	\N
145	0	1	42	14	13	\N
146	0	4	42	8	22	\N
147	1	1	42	15	35	\N
148	1	1	42	23	9	\N
149	2	0	42	18	28	\N
150	0	2	42	4	5	\N
151	0	3	42	44	29	\N
152	2	1	42	10	7	\N
153	1	1	42	3	20	\N
154	0	0	43	6	33	\N
155	3	5	43	17	16	\N
156	1	2	43	11	12	\N
157	0	4	43	35	13	\N
158	0	0	43	15	22	\N
159	0	3	43	8	14	\N
160	1	4	43	28	5	\N
161	0	0	43	23	4	\N
162	2	1	43	9	18	\N
163	1	1	43	10	20	\N
164	3	0	43	3	44	\N
165	3	0	43	7	29	\N
166	3	0	44	16	12	\N
167	0	2	44	17	6	\N
168	2	2	44	33	11	\N
169	2	0	44	22	35	\N
170	1	0	44	8	13	\N
171	0	0	44	14	15	\N
172	1	0	44	18	4	\N
173	1	1	44	9	28	\N
174	2	0	44	5	23	\N
175	0	2	44	44	10	\N
176	2	3	44	20	7	\N
177	0	3	44	29	3	\N
178	0	7	45	17	12	\N
179	1	0	45	6	11	\N
180	1	8	45	33	16	\N
181	2	1	45	22	14	\N
182	1	0	45	35	8	\N
183	1	1	45	13	15	\N
184	2	0	45	23	18	\N
185	0	3	45	9	5	\N
186	1	0	45	28	4	\N
187	3	1	45	29	20	\N
188	1	0	45	7	44	\N
189	0	0	45	3	10	\N
190	4	0	46	16	11	\N
191	3	2	46	12	6	\N
192	1	3	46	33	17	\N
193	1	2	46	15	8	\N
194	1	0	46	14	35	\N
195	2	1	46	13	22	\N
196	2	0	46	5	18	\N
197	0	1	46	4	9	\N
198	1	1	46	28	23	\N
199	0	0	46	7	3	\N
200	1	2	46	29	10	\N
201	2	0	46	20	44	\N
202	4	2	47	16	29	\N
203	3	2	47	13	18	\N
204	3	1	47	22	17	\N
205	0	3	48	9	3	\N
206	3	2	48	5	8	\N
207	3	1	48	7	6	\N
208	3	1	49	10	23	\N
209	2	1	49	12	14	\N
210	1	5	49	18	16	\N
211	1	2	50	29	13	\N
212	3	2	50	3	22	\N
213	3	1	50	17	9	\N
214	2	3	51	6	5	\N
215	0	1	51	8	7	\N
216	2	3	51	14	10	\N
217	0	1	52	23	12	\N
218	1	4	52	18	29	\N
219	3	1	52	16	13	\N
220	2	0	53	22	9	\N
221	2	1	53	3	17	\N
222	0	1	53	6	8	\N
223	0	1	54	5	7	\N
224	2	0	54	10	12	\N
225	3	2	54	14	23	\N
226	3	1	55	16	22	\N
227	2	0	55	3	5	\N
228	0	0	55	7	12	10
229	4	1	55	10	13	\N
230	2	2	56	16	3	11
231	0	2	56	7	10	\N
232	0	0	57	7	3	12
233	0	3	58	16	10	\N
234	2	1	59	3	37	\N
235	1	3	59	44	18	\N
236	2	2	59	21	24	\N
237	0	2	59	17	15	\N
238	3	0	59	10	43	\N
239	0	2	59	39	20	\N
240	0	4	59	45	30	\N
241	0	2	59	40	23	\N
242	1	2	59	34	35	\N
243	3	1	59	26	11	\N
244	1	1	59	4	28	\N
245	0	4	59	42	32	\N
246	1	0	59	25	2	\N
247	0	4	59	41	8	\N
248	1	2	59	14	19	\N
249	4	2	59	31	36	\N
250	5	3	59	18	3	\N
251	0	3	59	37	9	\N
252	1	2	59	15	21	\N
253	2	1	59	24	38	\N
254	2	3	59	20	10	\N
255	0	2	59	43	7	\N
256	6	0	59	23	45	\N
257	2	3	59	30	12	\N
258	2	3	59	11	34	\N
259	1	4	59	35	13	\N
260	0	3	59	32	4	\N
261	2	2	59	28	5	\N
262	2	0	59	8	25	\N
263	0	1	59	2	6	\N
264	0	1	59	36	14	\N
265	2	2	59	19	27	\N
266	1	4	59	9	18	\N
267	3	1	59	3	44	\N
269	3	1	59	21	17	\N
270	3	4	59	7	20	\N
271	2	0	59	10	39	\N
272	2	1	59	12	23	\N
273	1	2	59	45	40	\N
274	2	2	59	13	11	\N
275	3	3	59	34	26	\N
276	4	1	59	5	32	\N
277	3	0	59	4	42	\N
278	4	1	59	6	8	\N
279	3	1	59	25	41	\N
280	0	3	59	27	36	\N
281	0	1	59	14	31	\N
282	1	2	59	44	9	\N
283	1	2	59	18	37	\N
284	0	1	59	17	38	\N
285	4	0	59	15	24	\N
286	0	1	59	39	7	\N
287	9	1	59	20	43	\N
288	0	4	59	40	12	\N
289	4	0	59	23	30	\N
290	0	0	59	26	13	\N
291	1	0	59	11	35	\N
292	0	3	59	42	5	\N
293	2	1	59	32	28	\N
294	1	4	59	41	6	\N
295	4	2	59	8	2	\N
296	1	3	59	31	27	\N
297	1	2	59	36	19	\N
298	4	1	59	37	44	\N
299	2	3	59	9	3	\N
300	5	0	59	24	17	\N
301	0	3	59	38	21	\N
302	1	2	59	43	39	\N
303	0	4	59	7	10	\N
304	3	1	59	30	40	\N
305	7	0	59	12	45	\N
306	0	1	59	35	26	\N
307	2	2	59	13	34	\N
308	1	0	59	28	42	\N
309	0	1	59	5	4	\N
310	1	1	59	2	41	\N
311	1	0	59	6	25	\N
312	2	1	59	19	31	\N
313	4	1	59	27	14	\N
314	1	0	60	18	28	\N
315	0	2	60	27	25	\N
316	2	0	60	21	2	\N
317	2	1	60	3	31	\N
318	2	0	60	10	36	\N
319	1	1	60	15	9	\N
320	1	0	60	12	37	\N
321	2	2	60	20	24	\N
322	1	1	60	26	14	\N
323	3	1	60	23	7	\N
324	2	0	60	4	39	\N
325	2	1	60	13	30	\N
326	6	0	60	6	40	\N
327	3	1	60	5	34	\N
328	4	0	60	19	11	\N
329	3	0	60	8	32	\N
330	1	1	61	25	18	\N
331	1	2	61	28	27	\N
332	0	4	61	31	21	\N
333	0	4	61	2	3	\N
334	0	0	61	9	10	\N
335	0	2	61	36	15	\N
336	2	0	61	24	12	\N
337	1	4	61	37	20	\N
338	0	1	61	7	26	\N
339	3	1	61	14	23	\N
340	1	6	61	30	4	\N
341	1	3	61	39	13	\N
342	0	6	61	34	6	\N
343	1	4	61	40	5	\N
344	0	3	61	32	19	\N
345	0	1	61	11	8	\N
346	2	2	62	18	27	\N
347	2	0	62	25	28	\N
348	0	0	62	21	3	\N
349	2	1	62	31	2	\N
350	2	0	62	10	15	\N
352	1	2	62	9	36	\N
353	2	1	62	12	20	\N
354	2	0	62	24	37	\N
355	3	3	62	26	23	\N
356	1	2	62	7	14	\N
357	5	1	62	4	13	\N
358	2	0	62	30	39	\N
359	0	1	62	6	5	\N
360	3	2	62	34	40	\N
361	4	1	62	19	8	\N
362	2	1	62	32	11	\N
363	4	1	63	3	19	\N
364	4	0	63	4	8	\N
365	2	3	63	21	12	\N
366	2	0	63	15	24	\N
367	1	1	63	10	18	13
368	3	2	63	13	25	\N
369	2	0	63	5	26	\N
370	2	4	63	14	6	\N
371	5	0	64	3	10	\N
372	1	2	64	13	15	\N
373	2	2	64	5	6	14
374	2	0	64	4	12	\N
375	0	1	65	5	15	\N
376	2	1	65	3	4	\N
377	0	2	66	5	4	\N
378	1	0	67	15	3	\N
379	1	0	68	14	18	\N
380	0	1	68	47	36	\N
381	2	1	68	36	18	\N
382	1	0	68	18	47	\N
383	1	7	68	47	14	\N
384	0	2	68	36	14	\N
385	3	1	68	27	48	\N
386	1	4	68	34	46	\N
387	3	1	68	46	27	\N
388	1	4	68	34	48	\N
389	2	1	68	27	34	\N
390	0	0	68	48	46	\N
391	2	3	68	15	2	\N
392	1	1	68	77	32	\N
393	1	2	68	80	79	\N
394	1	1	68	71	69	\N
395	0	0	68	71	69	\N
396	4	4	68	20	11	\N
397	1	1	68	53	49	\N
398	1	0	68	58	57	\N
399	2	2	68	61	62	\N
400	2	0	68	4	63	\N
401	0	3	68	43	8	\N
402	0	0	68	10	26	\N
403	0	0	68	55	54	\N
404	0	1	68	56	17	\N
405	2	0	68	9	40	\N
406	0	0	68	76	19	\N
407	1	1	68	30	74	\N
408	1	1	68	73	75	\N
409	4	1	68	77	79	\N
410	2	3	68	66	41	\N
411	3	0	68	68	65	\N
412	2	1	68	55	9	\N
413	0	2	68	70	3	\N
414	2	0	68	39	72	\N
415	1	0	68	13	28	\N
416	2	2	68	51	52	\N
417	3	3	68	50	35	\N
418	0	6	68	62	63	\N
419	4	1	68	66	64	\N
420	0	2	68	60	4	\N
421	2	0	68	61	43	\N
422	1	3	68	20	49	\N
423	1	3	68	11	51	\N
424	0	0	68	35	53	\N
425	2	1	68	52	50	\N
426	7	0	68	12	68	\N
427	2	5	68	67	65	\N
428	0	4	68	57	59	\N
429	0	2	68	6	71	\N
430	1	2	68	15	80	\N
431	2	6	68	78	2	\N
432	1	1	68	25	19	\N
433	0	2	68	73	74	\N
434	0	3	68	76	30	\N
435	2	0	68	26	56	\N
436	2	0	68	60	63	\N
437	3	1	68	54	17	\N
438	2	2	68	40	10	\N
439	0	3	68	70	6	\N
440	1	1	68	3	69	\N
441	1	0	68	71	72	\N
442	2	3	68	7	13	\N
443	1	1	68	49	35	\N
444	4	1	68	59	28	\N
445	0	0	68	69	70	\N
446	7	0	68	63	43	\N
447	1	1	68	8	60	\N
448	1	4	68	62	4	\N
449	2	5	68	65	66	\N
450	2	0	68	41	67	\N
451	0	1	68	32	15	\N
452	5	1	68	77	78	\N
453	2	1	68	2	80	\N
454	2	4	68	54	10	\N
455	2	1	68	74	19	\N
456	1	1	68	25	75	\N
457	1	1	68	30	73	\N
458	2	1	68	11	52	\N
459	2	1	68	51	53	\N
460	0	2	68	20	35	\N
461	0	0	68	50	49	\N
462	1	2	68	71	39	\N
463	1	1	68	69	6	\N
464	5	0	68	3	72	\N
465	2	0	68	4	61	\N
466	1	3	68	62	60	\N
467	3	2	68	64	68	\N
468	2	0	68	9	56	\N
469	2	2	68	26	40	\N
470	0	0	68	55	17	\N
471	0	2	68	28	58	\N
472	1	0	68	12	64	\N
473	5	0	68	68	67	\N
474	3	3	68	79	15	\N
475	2	0	68	75	76	\N
476	0	1	68	57	13	\N
477	0	1	68	56	54	\N
478	2	0	68	55	10	\N
479	1	3	68	8	63	\N
480	7	1	68	80	78	\N
481	1	2	68	2	32	\N
482	2	2	68	35	11	\N
483	3	4	68	52	49	\N
484	2	2	68	50	51	\N
485	0	5	68	53	20	\N
486	0	1	68	25	73	\N
487	2	2	68	58	7	\N
488	2	0	68	19	30	\N
489	0	4	68	72	69	\N
490	4	0	68	6	39	\N
491	2	1	68	71	70	\N
492	2	0	68	60	61	\N
493	1	1	68	8	4	\N
494	2	2	68	43	62	\N
495	1	0	68	40	54	\N
496	1	3	68	17	26	\N
497	1	0	68	10	9	\N
498	0	3	68	56	55	\N
499	0	5	68	67	12	\N
500	1	4	68	41	65	\N
501	2	0	68	3	39	\N
502	0	0	68	17	40	\N
503	1	2	68	7	28	\N
504	0	1	68	61	8	\N
505	2	1	68	64	41	\N
506	4	1	68	79	2	\N
507	2	2	68	80	77	\N
508	1	2	68	78	32	\N
509	1	0	68	73	76	\N
510	0	2	68	30	75	\N
511	0	1	68	25	74	\N
512	1	3	68	43	4	\N
513	0	3	68	57	7	\N
514	1	2	68	59	13	\N
515	2	1	68	3	71	\N
516	1	1	68	39	70	\N
517	3	0	68	6	72	\N
518	0	2	68	49	51	\N
519	3	2	68	20	50	\N
520	3	1	68	11	53	\N
521	0	0	68	9	26	\N
522	0	6	68	53	52	\N
523	0	0	68	17	9	\N
524	1	2	68	56	10	\N
525	0	0	68	40	55	\N
526	1	1	68	54	26	\N
527	6	1	68	12	41	\N
528	3	2	68	68	66	\N
529	3	2	68	50	11	\N
530	0	4	68	78	79	\N
531	3	0	68	15	77	\N
532	0	3	68	75	74	\N
533	0	3	68	76	25	\N
534	0	3	68	19	73	\N
535	0	0	68	43	60	\N
536	3	0	68	63	61	\N
537	3	2	68	65	64	\N
538	0	2	68	54	9	\N
539	2	0	68	59	7	\N
540	2	1	68	10	17	\N
541	1	1	68	58	13	\N
542	0	1	68	6	3	\N
543	2	1	68	72	70	\N
544	5	1	68	39	69	\N
545	3	3	68	53	50	\N
546	1	2	68	35	51	\N
547	3	0	68	49	11	\N
548	0	3	68	52	20	\N
549	2	0	68	15	78	\N
550	1	1	68	79	32	\N
551	0	4	68	77	2	\N
552	2	3	68	41	68	\N
553	1	1	68	65	12	\N
554	0	3	68	67	66	\N
555	0	0	68	26	55	\N
556	3	2	68	40	56	\N
557	2	1	68	32	80	\N
558	1	2	68	51	20	\N
559	0	2	68	19	75	\N
560	2	1	68	74	76	\N
561	2	3	68	25	30	\N
562	1	1	68	8	62	\N
563	3	0	68	64	67	\N
564	1	1	68	52	35	\N
565	0	3	68	57	28	\N
566	0	5	68	58	59	\N
567	6	1	68	66	12	\N
568	1	0	69	60	36	\N
569	0	4	69	27	4	\N
570	4	4	69	59	79	\N
571	5	0	69	26	73	\N
572	6	0	69	14	32	\N
573	0	0	69	46	58	\N
574	1	1	69	63	12	\N
575	8	0	69	3	47	\N
576	2	4	69	66	51	\N
577	1	1	69	10	68	\N
578	0	2	69	2	55	\N
579	0	3	69	49	13	\N
580	1	4	69	13	2	\N
581	0	0	69	55	75	\N
582	3	0	69	49	71	\N
583	6	1	69	20	18	\N
584	1	2	69	6	74	\N
585	1	7	69	73	59	\N
586	4	2	69	79	27	\N
587	1	1	69	4	26	\N
588	3	0	69	32	46	\N
589	2	0	69	63	14	\N
590	1	4	69	68	74	\N
591	2	0	69	18	6	\N
592	2	2	69	10	20	\N
593	0	2	69	71	2	\N
594	2	0	69	49	75	\N
595	1	2	69	13	55	\N
596	0	0	69	51	3	\N
597	3	0	69	36	66	\N
598	2	3	69	47	60	\N
599	1	6	69	68	20	\N
600	2	0	69	6	10	\N
601	1	0	69	74	18	\N
602	3	0	69	71	55	\N
603	0	0	69	75	13	\N
604	0	3	69	2	49	\N
605	3	0	69	12	58	\N
606	1	4	69	36	47	\N
607	3	0	69	51	60	\N
608	0	3	69	66	3	\N
609	2	2	69	79	4	\N
610	2	3	69	6	68	\N
611	1	6	69	74	20	\N
612	1	1	69	18	10	\N
613	0	3	69	63	32	\N
614	3	0	69	63	32	\N
615	3	0	69	14	12	\N
616	3	2	69	46	63	\N
617	2	3	69	58	32	\N
618	1	1	69	79	73	\N
619	2	0	69	4	59	\N
620	2	4	69	26	27	\N
621	0	3	69	66	60	\N
622	3	1	69	3	36	\N
623	0	8	69	47	51	\N
624	3	1	69	75	71	\N
625	1	0	69	27	73	\N
626	4	2	69	59	26	\N
627	0	1	69	13	71	\N
628	1	1	69	55	49	\N
629	1	0	69	75	2	\N
630	3	0	69	47	66	\N
631	3	0	69	51	36	\N
632	3	1	69	12	46	\N
633	1	5	69	58	14	\N
634	1	1	69	20	6	\N
635	4	0	69	74	10	\N
636	1	4	69	18	68	\N
637	3	0	69	14	46	\N
638	1	4	69	12	32	\N
639	1	5	69	58	63	\N
640	1	3	69	3	60	\N
641	3	2	69	79	26	\N
642	0	2	69	27	59	\N
643	0	1	69	73	4	\N
644	1	2	70	7	39	\N
645	0	1	70	35	15	\N
646	3	1	70	61	30	\N
647	1	2	70	77	9	\N
648	2	1	70	40	52	\N
649	2	2	70	25	65	15
650	1	2	70	8	69	\N
651	1	3	70	64	28	\N
652	0	2	70	40	69	\N
653	1	2	70	9	15	\N
654	2	1	70	25	28	\N
655	0	1	70	61	39	\N
656	4	3	70	15	25	\N
657	0	2	70	69	39	\N
658	1	2	70	39	15	\N
659	0	1	71	14	3	\N
660	0	2	71	79	75	\N
661	1	2	71	68	32	\N
662	1	0	71	74	15	\N
663	3	2	71	59	51	\N
664	0	1	71	20	55	\N
665	2	1	71	4	60	\N
666	1	2	71	49	63	\N
667	7	1	71	59	74	\N
668	2	4	71	75	63	\N
669	0	1	71	55	3	\N
670	0	2	71	4	32	\N
671	2	3	71	63	3	\N
672	0	5	71	32	59	\N
673	6	1	71	63	32	\N
674	0	1	71	59	3	\N
\.


--
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: mark_database_admin
--

COPY public.player (id, name, points, "goalsScored", "goalsConceded", wins, draws, loses, titles, "highestWinId") FROM stdin;
39	Hugo Lichtenstein	22	19	25	7	1	9	0	544
52	Paul Matzke	8	16	15	2	2	4	0	522
79	Tim Gümpel	20	29	23	5	5	2	0	530
49	Jan Kunze	22	22	14	6	4	3	0	547
65	Felix Engel	11	17	16	3	2	2	0	427
8	Julius Schubert	64	55	75	17	13	21	0	247
28	Leon Heimer	19	20	29	5	4	10	0	565
37	Karl Mahn	6	8	14	2	0	5	0	298
47	Johannes Gabler	6	10	29	2	0	6	0	606
23	Paul Klukas	19	27	20	5	4	6	0	256
22	Lukas Beyer	16	17	10	5	1	3	0	146
78	Danny Jantowski	0	5	26	0	0	6	0	\N
5	Mark Heimer	82	75	47	25	7	13	0	160
27	Hannes Egert	23	26	29	7	2	6	0	313
64	Alisia Stock	9	12	14	3	0	4	0	563
18	Laurin Jeßnitzer	28	33	44	8	4	12	0	266
41	Alexander Fischer	7	13	29	2	1	7	0	450
12	Tim Doyé	60	67	36	19	3	7	0	178
25	Armin Juraschek	22	25	22	6	4	7	0	533
4	Max Gros	112	102	45	35	7	15	0	340
38	Ali Salhi	3	2	5	1	0	2	0	284
40	Céline Eckstein	16	16	34	4	4	8	0	273
20	Oliver Mälzer	42	70	39	12	6	7	0	287
9	Paul Gerth	63	58	57	17	12	19	0	251
69	Nico Seidel	14	12	11	3	5	2	0	489
14	Fabian Baur	52	57	40	16	4	15	0	383
61	Philipp Prechtl	7	7	12	2	1	5	0	421
59	Kurt Kirstenpfad	31	48	16	10	1	3	0	585
1	Niclas Landmann	7	7	17	1	4	4	0	22
68	Jesse Kmoch	19	27	30	6	1	5	0	473
3	Adrian Baur	156	150	42	48	12	6	5	38
75	Martin Maier	22	16	12	6	4	3	0	475
21	Chris Weber	17	18	7	5	2	1	0	332
15	Tobias Neumann	48	40	24	14	6	6	1	285
51	Julian Deierl	27	34	14	8	3	2	0	623
24	Erik Böhme	14	15	11	4	2	2	0	300
63	Markus Kittler	31	46	20	10	1	5	0	446
60	Moritz Lott	23	19	12	7	2	3	0	621
16	Bao Nguyen	28	40	14	9	1	1	0	180
74	Tommy Himstedt	31	25	18	10	1	2	0	635
55	Marvin Stöbe	24	13	7	6	6	2	0	498
32	Felix Wenzel	38	35	45	12	2	9	0	245
29	Robin Weber	9	14	16	3	0	5	0	151
33	Jonas Kamprath	9	11	30	2	3	10	0	126
44	Leo Winter	0	4	33	0	0	15	0	\N
7	Valentin Schach	61	58	59	17	10	19	0	165
45	Sabine Baur	0	1	32	0	0	6	0	\N
54	Tristan Hartwig	8	7	9	2	2	3	0	437
34	Abel Cristu	8	16	30	2	2	6	0	258
11	Florian Geißler	58	72	79	16	10	21	0	23
42	Felix Reichenbach	0	0	11	0	0	4	0	\N
31	Benedikt Heilmann	9	10	14	3	0	4	0	249
48	Colin Wiechert	4	5	4	1	1	1	0	388
30	Peter Siegl	17	22	27	5	2	7	0	240
62	Louis Stöbe	3	7	18	0	3	3	0	\N
72	Piet Ruiter	3	2	16	1	0	5	0	543
57	Mario Etzold	0	0	12	0	0	5	0	\N
66	Florian Schröder	12	24	26	4	0	7	0	567
58	Arno Glotz	9	10	24	2	3	5	0	471
53	Otto Neef	3	6	20	0	3	4	0	\N
73	Toni Hördler	12	9	19	3	3	5	0	534
43	Thomas Godts	2	5	33	0	2	8	0	\N
50	Toni Ruiter	7	14	15	1	4	2	0	529
80	Leon Kirmse	7	14	10	2	1	3	0	480
17	Jakob Dittmar	34	31	80	8	10	24	0	192
70	Patrick Wohlfahrt	2	3	10	0	2	4	0	\N
56	Max Paul Doberenz	0	3	14	0	0	7	0	\N
46	Franz Trozowski	11	11	13	3	2	3	0	386
76	Niklas Werrmann	1	1	11	0	1	5	0	\N
19	Sinh Loc Ngo	24	24	18	7	3	4	0	328
13	Jonas Wolf	48	46	40	14	6	8	0	157
6	Jonas Brenner	76	74	61	22	10	24	0	326
36	Fips Hartmann	15	16	26	5	0	10	0	280
77	Abdulrahman Jammo	8	13	14	2	2	3	0	452
67	Marius Neinert	0	2	23	0	0	6	0	\N
10	Pascal Kny	66	56	33	19	9	4	1	303
26	Lukas Sosinka	28	32	26	6	10	4	0	571
2	Georg Kriesche	32	35	45	10	2	16	0	431
35	Alexander Petzold	15	15	25	3	6	8	0	460
71	Cedric Künstler	17	13	14	5	2	5	0	602
\.


--
-- Data for Name: round; Type: TABLE DATA; Schema: public; Owner: mark_database_admin
--

COPY public.round (id, title, "tournamentId") FROM stdin;
1	Spieltag 1	1
2	Spieltag 2	1
3	Spieltag 3	1
4	Spieltag 4	1
5	Spieltag 5	1
6	Viertelfinale	1
7	Halbfinale	1
8	Finale	1
9	Spiel um Platz 3	1
10	Spieltag 1	2
11	Spieltag 2	2
12	Spieltag 3	2
13	Viertelfinale	2
14	Halbfinale	2
15	Finale	2
16	Spieltag 1	3
17	Spieltag 2	3
18	Spieltag 3	3
19	PlayOff`s	3
20	Viertelfinale	3
21	Lucky Loser Achtelfinale	3
22	Halbfinale	3
23	Lucky Loser Viertelfinale	3
24	Lucky Loser Halbfinale	3
25	Lucky Loser Finale	3
26	Kleines Finale	3
27	PlayOff Finale	3
28	Finale	3
29	Spieltag 1	4
30	Spieltag 2	4
31	Spieltag 3	4
32	Spieltag 4	4
33	Spieltag 5	4
34	Spieltag 6	4
35	Spieltag 7	4
36	Spieltag 8	4
37	Spieltag 9	4
38	Spieltag 10	4
39	Viertelfinale	4
40	Halbfinale	4
41	Finale	4
42	Vorrunde - Spieltag 1	5
43	Vorrunde - Spieltag 2	5
44	Vorrunde - Spieltag 3	5
45	Vorrunde - Spieltag 4	5
46	Vorrunde - Spieltag 5	5
47	Hauptrunde - Spieltag 1	5
48	Hauptrunde - Spieltag 2	5
49	Hauptrunde - Spieltag 3	5
50	Hauptrunde - Spieltag 4	5
51	Hauptrunde - Spieltag 5	5
52	Hauptrunde - Spieltag 6	5
53	Hauptrunde - Spieltag 7	5
54	Hauptrunde - Spieltag 8	5
55	Viertelfinale	5
56	Halbfinale	5
57	Spiel um Platz 3	5
58	Finale	5
59	Spieltag 1	6
60	2. Gruppenphase - Spieltag 1	6
61	2. Gruppenphase - Spieltag 2	6
62	2. Gruppenphase - Spieltag 3	6
63	Achtelfinale	6
64	Viertelfinale	6
65	Halbfinale	6
66	Spiel um Platz 3	6
67	Finale	6
68	Vorrunde	7
69	2. Gruppenphase	7
70	Lucky Loser - KO Phase	7
71	Achtelfinale	7
\.


--
-- Data for Name: tournament; Type: TABLE DATA; Schema: public; Owner: mark_database_admin
--

COPY public.tournament (id, date) FROM stdin;
1	2015-10-17
2	2017-10-21
3	2016-10-22
4	2018-10-20
5	2019-10-19
6	2020-10-17
7	2021-11-06
\.


--
-- Name: additional_information_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mark_database_admin
--

SELECT pg_catalog.setval('public.additional_information_id_seq', 15, true);


--
-- Name: game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mark_database_admin
--

SELECT pg_catalog.setval('public.game_id_seq', 674, true);


--
-- Name: player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mark_database_admin
--

SELECT pg_catalog.setval('public.player_id_seq', 80, true);


--
-- Name: round_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mark_database_admin
--

SELECT pg_catalog.setval('public.round_id_seq', 71, true);


--
-- Name: tournament_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mark_database_admin
--

SELECT pg_catalog.setval('public.tournament_id_seq', 7, true);


--
-- Name: round PK_34bd959f3f4a90eb86e4ae24d2d; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.round
    ADD CONSTRAINT "PK_34bd959f3f4a90eb86e4ae24d2d" PRIMARY KEY (id);


--
-- Name: game PK_352a30652cd352f552fef73dec5; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "PK_352a30652cd352f552fef73dec5" PRIMARY KEY (id);


--
-- Name: tournament PK_449f912ba2b62be003f0c22e767; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.tournament
    ADD CONSTRAINT "PK_449f912ba2b62be003f0c22e767" PRIMARY KEY (id);


--
-- Name: player PK_65edadc946a7faf4b638d5e8885; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT "PK_65edadc946a7faf4b638d5e8885" PRIMARY KEY (id);


--
-- Name: additional_information PK_7f23a52f08f1167507f8ec6a94d; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.additional_information
    ADD CONSTRAINT "PK_7f23a52f08f1167507f8ec6a94d" PRIMARY KEY (id);


--
-- Name: game REL_046647471104e08d61208eee87; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "REL_046647471104e08d61208eee87" UNIQUE ("additionalInformationId");


--
-- Name: player REL_dae5ad8d4531e6ff4978f188e4; Type: CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT "REL_dae5ad8d4531e6ff4978f188e4" UNIQUE ("highestWinId");


--
-- Name: game FK_046647471104e08d61208eee878; Type: FK CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "FK_046647471104e08d61208eee878" FOREIGN KEY ("additionalInformationId") REFERENCES public.additional_information(id);


--
-- Name: game FK_27961a05f7c2f273d655ea2784c; Type: FK CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "FK_27961a05f7c2f273d655ea2784c" FOREIGN KEY ("roundId") REFERENCES public.round(id);


--
-- Name: game FK_621eb937fa0bf5e57916bc59d84; Type: FK CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "FK_621eb937fa0bf5e57916bc59d84" FOREIGN KEY ("homePlayerId") REFERENCES public.player(id);


--
-- Name: game FK_8bed63f9f1acfe9dbb82b58f77d; Type: FK CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT "FK_8bed63f9f1acfe9dbb82b58f77d" FOREIGN KEY ("awayPlayerId") REFERENCES public.player(id);


--
-- Name: round FK_a019f16925812eb8b9cd5ff8100; Type: FK CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.round
    ADD CONSTRAINT "FK_a019f16925812eb8b9cd5ff8100" FOREIGN KEY ("tournamentId") REFERENCES public.tournament(id);


--
-- Name: player FK_dae5ad8d4531e6ff4978f188e42; Type: FK CONSTRAINT; Schema: public; Owner: mark_database_admin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT "FK_dae5ad8d4531e6ff4978f188e42" FOREIGN KEY ("highestWinId") REFERENCES public.game(id);


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

