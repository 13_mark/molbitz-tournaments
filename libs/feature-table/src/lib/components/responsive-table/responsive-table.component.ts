import { Component, Input, OnInit } from '@angular/core';

import { IPlayer } from '@molbitz-tournaments/util-common';

interface HighestWinViewModel {
  goalsScored: number;
  goalsConceded: number;
  enemyName: string;
}

function calculatePlayerHighestWinViewModel(
  player: IPlayer
): HighestWinViewModel | undefined {
  const isHomePlayerInHighestWin = player.highestWin
    ? player.highestWin.homePlayerScore > player.highestWin.awayPlayerScore
      ? true
      : false
    : false;
  if (player.highestWin) {
    if (isHomePlayerInHighestWin) {
      return {
        goalsScored: player.highestWin.homePlayerScore,
        goalsConceded: player.highestWin.awayPlayerScore,
        enemyName: player.highestWin.awayPlayer.name
      };
    } else {
      return {
        goalsScored: player.highestWin.awayPlayerScore,
        goalsConceded: player.highestWin.homePlayerScore,
        enemyName: player.highestWin.homePlayer.name
      };
    }
  } else {
    return undefined;
  }
}

function calculatePlayerAttendances(player: IPlayer): number {
  const playerAllGames = [...player.homeGames, ...player.awayGames];
  return playerAllGames.reduce((accumulator, currentValue) => {
    if (
      currentValue.round.tournament &&
      !accumulator.includes(currentValue.round.tournament.id)
    ) {
      return [...accumulator, currentValue.round.tournament.id];
    }
    return accumulator;
  }, [] as number[]).length;
}

@Component({
  selector: 'feature-table-responsive-table',
  templateUrl: './responsive-table.component.html',
  styleUrls: ['./responsive-table.component.scss']
})
export class ResponsiveTableComponent implements OnInit {
  @Input()
  set dataSource(players: IPlayer[]) {
    this.tablePlayerViewModel = players.map((player) => ({
      ...player,
      highestWinViewModel: calculatePlayerHighestWinViewModel(player),
      attendances: calculatePlayerAttendances(player)
    }));
  }

  tablePlayerViewModel: (IPlayer & {
    attendances: number;
    highestWinViewModel:
      | {
          goalsScored: number;
          goalsConceded: number;
          enemyName: string;
        }
      | undefined;
  })[] = [];

  constructor() {}

  ngOnInit(): void {}
}
