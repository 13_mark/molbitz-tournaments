import { HttpClient } from '@angular/common/http';
import { IPlayer } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';
import { map } from 'rxjs/operators';

function sortPlayersByPoints(a: IPlayer, b: IPlayer): number {
  if (a.points < b.points) {
    return 1;
  }
  if (a.points > b.points) {
    return -1;
  }
  const aGoalDifference = a.goalsScored - a.goalsConceded;
  const bGoalDifference = b.goalsScored - b.goalsConceded;
  if (a.points === b.points) {
    if (aGoalDifference > bGoalDifference) {
      return -1;
    }
    if (aGoalDifference < bGoalDifference) {
      return 1;
    }
  }
  return 0;
}

@Injectable({
  providedIn: 'root'
})
export class TableResolver implements Resolve<IPlayer[]> {
  constructor(private readonly http: HttpClient) {}

  resolve(): Observable<IPlayer[]> {
    return this.http
      .get<IPlayer[]>('players')
      .pipe(map((players) => players.sort(sortPlayersByPoints)));
  }
}
