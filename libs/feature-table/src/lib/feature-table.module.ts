import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './containers/table/table.component';
import { RouterModule } from '@angular/router';
import { ResponsiveTableComponent } from './components/responsive-table/responsive-table.component';
import { UiCommonModule } from '@molbitz-tournaments/ui-common';
import { TableResolver } from './resolvers/table.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: TableComponent,
        resolve: {
          tableDataSource: TableResolver
        }
      }
    ]),
    UiCommonModule
  ],
  declarations: [TableComponent, ResponsiveTableComponent],
  exports: [TableComponent]
})
export class FeatureTableModule {}
