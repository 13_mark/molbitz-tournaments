import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { IPlayer } from '@molbitz-tournaments/util-common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'feature-table-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
  tableDataSource: Observable<IPlayer[]>;

  constructor(private readonly routes: ActivatedRoute) {
    this.tableDataSource = this.routes.data.pipe(map((resp) => resp.tableDataSource));
  }
}
