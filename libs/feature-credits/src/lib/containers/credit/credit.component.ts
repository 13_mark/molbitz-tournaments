import { Component, OnInit } from '@angular/core';
import { navigateToUrl } from '@molbitz-tournaments/util-common';

@Component({
  selector: 'feature-credit-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  navigateToUrl(url: string) {
    navigateToUrl(url);
  }
}
