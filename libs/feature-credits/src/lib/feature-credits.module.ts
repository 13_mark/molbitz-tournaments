import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditComponent } from './containers/credit/credit.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

const APP_ROUTES: Routes = [
  {
    path: '',
    component: CreditComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(APP_ROUTES)],
  declarations: [CreditComponent]
})
export class FeatureCreditsModule {}
