import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MediaWithTextComponent } from './components/media-with-text/media-with-text.component';
import { HomeComponent } from './containers/home/home.component';
import { BorderedImageComponent } from './components/bordered-image/bordered-image.component';
import { ProfileImageComponent } from './components/profile-image/profile-image.component';
import { QuoteComponent } from './components/quote/quote.component';
import { UiCommonModule } from '@molbitz-tournaments/ui-common';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', pathMatch: 'full', component: HomeComponent }]),
    UiCommonModule
  ],
  declarations: [
    MediaWithTextComponent,
    HomeComponent,
    BorderedImageComponent,
    ProfileImageComponent,
    QuoteComponent
  ],
  exports: [HomeComponent]
})
export class FeatureHomeModule {}
