import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'feature-home-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.scss']
})
export class ProfileImageComponent implements OnInit {
  @Input() set profileImageUrl(url: string) {
    this.profileImageStyle = `url(${url})`;
  }

  profileImageStyle: string = 'url()';

  constructor() {}

  ngOnInit(): void {}
}
