import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'feature-home-media-with-text',
  templateUrl: './media-with-text.component.html',
  styleUrls: ['./media-with-text.component.scss']
})
export class MediaWithTextComponent implements OnInit {
  @Input() reverse = false;

  @Input() imageUrl?: string;

  @Input() profileImageUrl?: string;

  @Input() quote?: string;

  @Input() author?: string;

  constructor() {}

  ngOnInit(): void {}
}
