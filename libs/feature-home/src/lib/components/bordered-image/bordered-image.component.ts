import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'feature-home-bordered-image',
  templateUrl: './bordered-image.component.html',
  styleUrls: ['./bordered-image.component.scss']
})
export class BorderedImageComponent implements OnInit {
  @Input() isBorderLeftSide = true;
  @Input() set imageUrl(url: string) {
    this.backgroundImageStyle = `url(${url})`;
  }

  backgroundImageStyle?: string;

  constructor() {}

  ngOnInit(): void {}
}
