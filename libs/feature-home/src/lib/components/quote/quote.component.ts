import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'feature-home-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {
  @Input() author?: string;
  @Input() quote?: string;

  constructor() {}

  ngOnInit(): void {}
}
