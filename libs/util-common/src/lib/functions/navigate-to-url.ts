export function navigateToUrl(url: string) {
  window.open(url, '_blank');
}
