import { IAdditionalInformation } from './additional-information.type';
import { IPlayer } from './player.type';
import { IRound } from './round.type';

export interface IGame {
  id: number;
  homePlayerScore: number;
  awayPlayerScore: number;
  round: IRound;
  homePlayer: IPlayer;
  awayPlayer: IPlayer;
  additionalInformation: IAdditionalInformation | null;
}
