import { IGame } from './game.type';
import { ITournament } from './tournament.type';

export interface IRound {
  id: number;
  title: string;
  tournament?: ITournament;
  games?: IGame[];
}
