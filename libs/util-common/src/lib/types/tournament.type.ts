import { IRound } from './round.type';

export interface ITournament {
  id: number;
  date: Date;
  rounds?: IRound[];
}
