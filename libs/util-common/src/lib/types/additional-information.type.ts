export interface IAdditionalInformation {
  id: number;
  extraTimeHomePlayerScore: number;
  extraTimeAwayPlayerScore: number;
  penaltyShootoutHomePlayerScore?: number;
  penaltyShootoutAwayPlayerScore?: number;
}
