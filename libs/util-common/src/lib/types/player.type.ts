import { IGame } from './game.type';

export interface IPlayer {
  id: number;
  name: string;
  points: number;
  goalsScored: number;
  goalsConceded: number;
  wins: number;
  draws: number;
  loses: number;
  titles: number;
  homeGames: IGame[];
  awayGames: IGame[];
  highestWin: IGame | null;
}
