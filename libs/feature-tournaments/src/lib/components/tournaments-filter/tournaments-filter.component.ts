import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'feature-tournaments-tournaments-filter',
  templateUrl: './tournaments-filter.component.html',
  styleUrls: ['./tournaments-filter.component.scss']
})
export class TournamentsFilterComponent implements OnInit {
  @Input() availableYears: string[] = [];
  @Input() initialYear?: string;
  @Output() selectedYearChanged = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  selectionChanged(newValue: string) {
    this.selectedYearChanged.emit(newValue);
  }
}
