import { Component, Input, OnInit } from '@angular/core';
import { ITournament } from '@molbitz-tournaments/util-common';

@Component({
  selector: 'feature-tournaments-tournament-games',
  templateUrl: './tournament-games.component.html',
  styleUrls: ['./tournament-games.component.scss']
})
export class TournamentGamesComponent implements OnInit {
  @Input() tournament?: ITournament;

  constructor() {}

  ngOnInit(): void {}
}
