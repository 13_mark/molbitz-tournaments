import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TournamentsComponent } from './containers/tournaments/tournaments.component';
import { UiCommonModule } from '@molbitz-tournaments/ui-common';
import { TournamentsFilterComponent } from './components/tournaments-filter/tournaments-filter.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { FormsModule } from '@angular/forms';
import { TournamentGamesComponent } from './components/tournament-games/tournament-games.component';
import { TournamentsResolver } from './resolvers/tournaments.resolver';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: TournamentsComponent,
        resolve: {
          tournamentsData: TournamentsResolver
        }
      }
    ]),
    UiCommonModule,
    NgxSliderModule,
    FormsModule
  ],
  declarations: [
    TournamentsComponent,
    TournamentsFilterComponent,
    TournamentGamesComponent
  ]
})
export class FeatureTournamentsModule {}
