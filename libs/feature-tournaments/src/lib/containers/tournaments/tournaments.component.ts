import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITournament } from '@molbitz-tournaments/util-common';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'feature-tournaments-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.scss']
})
export class TournamentsComponent {
  selectedYearsTournamentData$: Observable<ITournament | undefined>;
  tournamentsAvailableYears$: Observable<string[]>;

  readonly selectedYearChangedSubject: BehaviorSubject<
    string | undefined
  > = new BehaviorSubject<string | undefined>(undefined);

  constructor(private readonly routes: ActivatedRoute) {
    const allTournamentsData$: Observable<ITournament[]> = this.routes.data.pipe(
      map((resp) => resp.tournamentsData)
    );

    this.selectedYearsTournamentData$ = combineLatest([
      allTournamentsData$,
      this.selectedYearChangedSubject
    ]).pipe(
      map(([allTournaments, selectedYear]) =>
        allTournaments.find((tournament) => tournament.date.toString() === selectedYear)
      )
    );

    this.tournamentsAvailableYears$ = this.routes.data.pipe(
      map((resp) => resp.tournamentsData),
      map((tournaments: ITournament[]) =>
        tournaments.map((tournament) => tournament.date.toString())
      )
    );

    this.tournamentsAvailableYears$
      .pipe(map((items) => items[0]))
      .subscribe((initialYear) => this.selectedYearChangedSubject.next(initialYear));
  }

  onSelectedYearChanged(year: string) {
    this.selectedYearChangedSubject.next(year);
  }
}
