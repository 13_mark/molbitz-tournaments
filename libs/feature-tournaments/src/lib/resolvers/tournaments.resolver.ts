import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { IRound, ITournament } from '@molbitz-tournaments/util-common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

function sortTournamentsByDate(a: ITournament, b: ITournament): number {
  if (a.date < b.date) {
    return -1;
  }
  if (a.date > b.date) {
    return 1;
  }
  return 0;
}

function sortRoundsByIdAscending(a: IRound, b: IRound) {
  if (a.id < b.id) {
    return -1;
  }
  if (a.id > b.id) {
    return 1;
  }
  return 0;
}

@Injectable({
  providedIn: 'root'
})
export class TournamentsResolver implements Resolve<ITournament[]> {
  constructor(private readonly http: HttpClient) {}

  resolve(): Observable<ITournament[]> {
    return this.http.get<ITournament[]>('tournaments').pipe(
      map((tournaments: ITournament[]) => tournaments.sort(sortTournamentsByDate)),
      map((tournaments: ITournament[]) => {
        tournaments.forEach((tournament) =>
          tournament.rounds?.sort(sortRoundsByIdAscending)
        );
        return tournaments;
      })
    );
  }
}
