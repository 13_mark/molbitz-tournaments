import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-common-header-image',
  templateUrl: './header-image.component.html',
  styleUrls: ['./header-image.component.scss']
})
export class HeaderImageComponent implements OnInit {
  @Input() set imageUrl(url: string) {
    this.backgroundImageStyle = `url(${url})`;
  }

  @Input() headerText: string = 'Molbitzer-Turniere';

  imageIsLoaded = false;
  backgroundImageStyle?: string;

  constructor() {}

  ngOnInit(): void {}

  onLoad() {
    this.imageIsLoaded = true;
  }
}
