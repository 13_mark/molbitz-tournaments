import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-common-info-banner',
  templateUrl: './info-banner.component.html',
  styleUrls: ['./info-banner.component.scss']
})
export class InfoBannerComponent implements OnInit {
  @Input() heading?: string;

  @Input() text?: string;

  constructor() {}

  ngOnInit(): void {}
}
