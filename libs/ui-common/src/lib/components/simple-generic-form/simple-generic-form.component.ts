import {
  Component,
  ContentChild,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'ui-common-simple-generic-form',
  templateUrl: './simple-generic-form.component.html',
  styleUrls: ['./simple-generic-form.component.scss']
})
export class SimpleGenericFormComponent<T> implements OnInit, OnDestroy {
  @ContentChild(TemplateRef, { static: false }) templateRef?: TemplateRef<unknown>;

  @Input()
  formGroup?: FormGroup;

  @Input()
  submitFunction?: (formValues: T) => Observable<unknown>;

  @Input()
  submitMessageText?: string;

  @Input()
  errorMessageText?: string;

  @Input()
  inlineForm = true;

  @Output() formSuccessfullSubmitted = new EventEmitter<any>();

  formIsLoadingState = false;
  formSubmissionState: 'NotYetSubmitted' | 'Success' | 'Failure' = 'NotYetSubmitted';
  subscriptions: Subscription[] = [];

  constructor() {}

  ngOnInit(): void {}

  onSubmit() {
    if (this.submitFunction) {
      this.formIsLoadingState = true;
      this.subscriptions.push(
        this.submitFunction(this.formGroup?.value).subscribe(
          (response) => {
            this.formSubmissionState = 'Success';
            this.formIsLoadingState = false;
            this.formSuccessfullSubmitted.emit(response);
          },
          () => {
            this.formSubmissionState = 'Failure';
            this.formIsLoadingState = false;
          }
        )
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
