import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'ui-common-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @ContentChild(TemplateRef, { static: false }) templateRef?: TemplateRef<unknown>;

  @Input()
  cardHeader = 'Sample Card Header';

  constructor() {}

  ngOnInit(): void {}
}
