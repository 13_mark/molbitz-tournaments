import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'ui-common-table-data-list-with-action',
  templateUrl: './table-data-list-with-action.component.html',
  styleUrls: ['./table-data-list-with-action.component.scss']
})
export class TableDataListWithActionComponent<T> implements OnInit {
  @ContentChild(TemplateRef, { static: false }) templateRef?: TemplateRef<unknown>;

  @Input()
  dataSource: ReadonlyArray<T> = [];

  @Input()
  tableHeaderDisplayNames: ReadonlyArray<string> = [];

  @Input()
  dataSourceDisplayProperties: ReadonlyArray<keyof T> = [];

  @Input()
  tableActionIdentifier?: keyof T;

  constructor() {}

  ngOnInit(): void {}
}
