import { Component, OnDestroy } from '@angular/core';
import { ResolveEnd, ResolveStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ui-common-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnDestroy {
  burgerMenuOpen = false;
  loadingIndicator = false;
  private readonly subscriptions: Subscription[] = [];

  constructor(private readonly router: Router) {
    this.subscriptions.push(
      this.router.events.subscribe((routerEvent) => {
        if (routerEvent instanceof ResolveStart) {
          this.loadingIndicator = true;
        }

        if (routerEvent instanceof ResolveEnd) {
          this.loadingIndicator = false;
        }
      })
    );
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  toggleBurgerMenu() {
    this.burgerMenuOpen = !this.burgerMenuOpen;
  }
}
