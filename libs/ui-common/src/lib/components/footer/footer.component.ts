import { Component, OnInit } from '@angular/core';
import { navigateToUrl } from '@molbitz-tournaments/util-common';

@Component({
  selector: 'ui-common-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  navigateToUrl(url: string) {
    navigateToUrl(url);
  }
}
