import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppShellComponent } from './containers/app-shell/app-shell.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { InfoBannerComponent } from './components/info-banner/info-banner.component';
import { HeaderImageComponent } from './components/header-image/header-image.component';
import { DividerComponent } from './components/divider/divider.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { CardComponent } from './components/card/card.component';
import { TableDataListWithActionComponent } from './components/table-data-list/table-data-list-with-action.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SimpleGenericFormComponent } from './components/simple-generic-form/simple-generic-form.component';

@NgModule({
  imports: [CommonModule, RouterModule.forChild([]), ReactiveFormsModule],
  declarations: [
    AppShellComponent,
    NavbarComponent,
    FooterComponent,
    InfoBannerComponent,
    HeaderImageComponent,
    DividerComponent,
    LoadingSpinnerComponent,
    CardComponent,
    TableDataListWithActionComponent,
    SimpleGenericFormComponent
  ],
  exports: [
    AppShellComponent,
    InfoBannerComponent,
    HeaderImageComponent,
    DividerComponent,
    LoadingSpinnerComponent,
    CardComponent,
    TableDataListWithActionComponent,
    SimpleGenericFormComponent
  ]
})
export class UiCommonModule {}
