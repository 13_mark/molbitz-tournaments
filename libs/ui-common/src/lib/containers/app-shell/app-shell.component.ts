import { Component, ContentChild, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'ui-common-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent implements OnInit {
  @ContentChild(TemplateRef, { static: false }) templateRef?: TemplateRef<unknown>;

  constructor() {}

  ngOnInit(): void {}
}
