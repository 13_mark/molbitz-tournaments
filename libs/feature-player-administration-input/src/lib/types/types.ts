import { IGame, IPlayer, IRound, ITournament } from '@molbitz-tournaments/util-common';

import { IAdditionalInformation } from 'libs/util-common/src/lib/types/additional-information.type';

export type TournamentSubmitModel = Pick<ITournament, 'date'>;
export type RoundSubmitModel = Pick<IRound, 'title'> & {
  tournament: Pick<ITournament, 'id'>;
};
export type GameSubmitModel = Omit<IGame, 'id' | 'round' | 'additionalInformation'> & {
  round: Pick<IRound, 'id'>;
} & {
  additionalInformation: Omit<IAdditionalInformation, 'id'> | null;
};
export type PlayerSubmitModel = Pick<IPlayer, 'name'>;

export interface GamesTableDataSource {
  id: number;
  homePlayerName: string;
  awayPlayerName: string;
  homePlayerScore: number;
  awayPlayerScore: number;
}
