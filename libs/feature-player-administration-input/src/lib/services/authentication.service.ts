import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(
    private readonly http: HttpClient,
    private readonly tokenService: TokenService
  ) {}

  login(body: { username: string; password: string }): Observable<string> {
    const url = `authenticate`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<{ token: string }>(url, JSON.stringify(body), { headers })
      .pipe(
        map((response) => response.token),
        tap((token) => {
          this.tokenService.setToken(token);
        })
      );
  }
}
