import { HttpClient, HttpHeaders } from '@angular/common/http';

import { IRound, ITournament } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';
import { RoundSubmitModel } from '../types/types';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoundService {
  constructor(private readonly http: HttpClient) {}

  getRound(roundId: number, tournamentId: number): Observable<IRound> {
    return this.http
      .get<IRound>(`round/${roundId}`)
      .pipe(
        switchMap((round) =>
          this.http
            .get<ITournament>(`tournament/${tournamentId}`)
            .pipe(map((tournament) => ({ ...round, tournament })))
        )
      );
  }

  createRound(body: RoundSubmitModel) {
    const url = `round`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<IRound>(url, JSON.stringify(body), { headers });
  }
}
