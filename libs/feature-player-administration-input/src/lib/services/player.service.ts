import { HttpClient, HttpHeaders } from '@angular/common/http';

import { IPlayer } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayerSubmitModel } from '../types/types';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  constructor(private readonly http: HttpClient) {}

  createPlayer(body: PlayerSubmitModel) {
    const url = `player`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<IPlayer>(url, JSON.stringify(body), { headers });
  }

  updateTitles(requestData: { id: number; titles: number }): Observable<IPlayer> {
    const url = `player/${requestData.id}`;
    const headers = new HttpHeaders({ 'Content-Tye': 'application/json' });
    return this.http.put<IPlayer>(url, { titles: requestData.titles }, { headers });
  }
}
