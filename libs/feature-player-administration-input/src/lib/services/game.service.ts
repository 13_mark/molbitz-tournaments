import { HttpClient, HttpHeaders } from '@angular/common/http';

import { GameSubmitModel } from '../types/types';
import { IGame } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private readonly http: HttpClient) {}

  createGame(game: GameSubmitModel): Observable<IGame> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<IGame>(`game`, JSON.stringify(game), { headers });
  }

  deleteGame(gameId: number) {
    return this.http.delete<boolean>(`game/${gameId}`);
  }
}
