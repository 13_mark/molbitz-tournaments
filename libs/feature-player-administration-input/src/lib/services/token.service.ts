import { Injectable } from '@angular/core';

const ACCESS_STRING_LOCAL_STORAGE = 'player-admin-token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private token: string | undefined;

  constructor() {
    this.token = this.getToken();
  }

  setToken(token: string) {
    this.token = token;
    this.setTokenLocalStorage(token);
  }

  getToken(): string | undefined {
    if (!this.token) {
      const tokenLocalStorage = this.getTokenFromLocalStorage();
      if (tokenLocalStorage) {
        return tokenLocalStorage;
      } else {
        return undefined;
      }
    } else {
      return this.token;
    }
  }

  private getTokenFromLocalStorage(): string | null {
    return localStorage.getItem(ACCESS_STRING_LOCAL_STORAGE);
  }

  private setTokenLocalStorage(token: string): void {
    localStorage.setItem(ACCESS_STRING_LOCAL_STORAGE, token);
  }
}
