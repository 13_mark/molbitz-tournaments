import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ITournament } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TournamentSubmitModel } from '../types/types';

@Injectable({
  providedIn: 'root'
})
export class TournamentService {
  constructor(private readonly http: HttpClient) {}

  createTournament(body: TournamentSubmitModel): Observable<ITournament> {
    const url = `tournament`;
    const header = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ITournament>(url, JSON.stringify(body), {
      headers: header
    });
  }
}
