import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { TokenService } from '../services/token.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class PlayerAdministrationInterceptor implements HttpInterceptor {
  constructor(
    private readonly tokenService: TokenService,
    private readonly router: Router
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (!request.url.includes('authenticate')) {
      const token = this.tokenService.getToken();
      const updatedRequest = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`)
      });
      return next.handle(updatedRequest).pipe(
        catchError((err: HttpEvent<unknown>) => {
          console.error(err);
          this.router.navigate(['/login']);
          return of(err);
        })
      );
    } else {
      return next.handle(request);
    }
  }
}
