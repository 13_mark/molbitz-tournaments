import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PlayerDetailComponent } from './components/player-detail/player-detail.component';
import { PlayerDetailResolver } from './resolvers/player-detail.resolver';
import { PlayerResolver } from './resolvers/player.resolver';
import { PlayersInputComponent } from './containers/players-input/players-input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RoundDetailComponent } from './components/round-detail/round-detail.component';
import { RoundDetailResolver } from './resolvers/round-detail.resolver';
import { RouterModule } from '@angular/router';
import { TournamentDetailComponent } from './components/tournament-detail/tournament-detail.component';
import { TournamentDetailResolver } from './resolvers/tournament-detail.resolver';
import { TournamentResolver } from './resolvers/tournament.resolver';
import { TournamentsInputComponent } from './containers/tournaments-input/tournaments-input.component';
import { UiCommonModule } from '@molbitz-tournaments/ui-common';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'admin/tournaments',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: AdminLoginComponent
      },
      {
        path: 'admin',
        children: [
          {
            path: 'tournament/:tournamentId/round/:roundId',
            component: RoundDetailComponent,
            resolve: { roundDetailData: RoundDetailResolver, playersData: PlayerResolver }
          },
          {
            path: 'tournament/:id',
            component: TournamentDetailComponent,
            resolve: { tournamentsData: TournamentDetailResolver }
          },
          {
            path: 'tournaments',
            component: TournamentsInputComponent,
            resolve: { tournaments: TournamentResolver }
          },
          {
            path: 'players',
            component: PlayersInputComponent,
            resolve: { players: PlayerResolver }
          },
          {
            path: 'player/:id',
            component: PlayerDetailComponent,
            resolve: { playerDetailData: PlayerDetailResolver }
          }
        ]
      },
      {
        path: '**',
        redirectTo: 'admin/tournaments'
      }
    ]),
    ReactiveFormsModule,
    UiCommonModule,
    AutocompleteLibModule
  ],
  declarations: [
    TournamentsInputComponent,
    TournamentDetailComponent,
    RoundDetailComponent,
    PlayersInputComponent,
    PlayerDetailComponent,
    AdminLoginComponent
  ]
})
export class FeaturePlayerAdministrationInputModule {}
