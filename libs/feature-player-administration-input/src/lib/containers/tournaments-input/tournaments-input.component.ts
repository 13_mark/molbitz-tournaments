import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ITournament } from '@molbitz-tournaments/util-common';
import { TournamentService } from '../../services/tournament.service';

function computeCurrentDate(): string {
  const currentDate = new Date();
  const year = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(currentDate);
  const month = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(currentDate);
  const day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(currentDate);
  return `${year}-${month}-${day}`;
}

@Component({
  selector: 'feature-player-administration-input-tournaments-input',
  templateUrl: './tournaments-input.component.html',
  styleUrls: ['./tournaments-input.component.scss']
})
export class TournamentsInputComponent {
  allTournaments: ITournament[];
  submitFunction = this.submitMethod.bind(this);
  tournamentForm: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly tournamentService: TournamentService,
    private readonly route: ActivatedRoute
  ) {
    this.tournamentForm = this.fb.group({
      date: computeCurrentDate()
    });
    const data: ITournament[] = this.route.snapshot.data.tournaments;
    this.allTournaments = data.sort((a, b) => (a.date > b.date ? 1 : -1));
  }

  onFormSuccessfullSubmitted(createdTournament: ITournament) {
    this.allTournaments = [...this.allTournaments, createdTournament];
  }

  submitMethod(values: any) {
    return this.tournamentService.createTournament(values);
  }
}
