import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IPlayer } from '@molbitz-tournaments/util-common';
import { PlayerService } from '../../services/player.service';

@Component({
  selector: 'molbitz-tournaments-players-input',
  templateUrl: './players-input.component.html',
  styleUrls: ['./players-input.component.scss']
})
export class PlayersInputComponent {
  players: IPlayer[];
  playerForm: FormGroup;
  submitFunction = this.submitMethod.bind(this);

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly playerService: PlayerService
  ) {
    const data: IPlayer[] = this.route.snapshot.data.players;
    this.players = data.sort((a, b) => a.id - b.id);
    this.playerForm = this.fb.group({
      name: ''
    });
  }

  submitMethod(values: { name: string }) {
    return this.playerService.createPlayer(values);
  }

  onFormSuccessfullSubmitted(createdPlayer: IPlayer) {
    this.players = [...this.players, createdPlayer];
  }
}
