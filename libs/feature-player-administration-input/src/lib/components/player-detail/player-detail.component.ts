import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IPlayer } from '@molbitz-tournaments/util-common';
import { PlayerService } from '../../services/player.service';
import { GamesTableDataSource } from '../../types/types';

@Component({
  selector: 'molbitz-tournaments-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.scss']
})
export class PlayerDetailComponent {
  playerDetailData: IPlayer;
  playerGamesTableDataSource: GamesTableDataSource[];
  highestWinViewModel?: {
    opposingPlayer: string;
    thisPlayerScore: number;
    opposingPlayerScore: number;
  };
  titlesForm: FormGroup;
  submitFunction = this.submitMethod.bind(this);

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly playerService: PlayerService
  ) {
    this.playerDetailData = this.route.snapshot.data.playerDetailData;
    this.playerGamesTableDataSource = [
      ...this.playerDetailData.homeGames.map((game) => ({
        id: game.id,
        homePlayerName: game.homePlayer.name,
        awayPlayerName: game.awayPlayer.name,
        homePlayerScore: game.homePlayerScore,
        awayPlayerScore: game.awayPlayerScore
      })),
      ...this.playerDetailData.awayGames.map((game) => ({
        id: game.id,
        homePlayerName: game.homePlayer.name,
        awayPlayerName: game.awayPlayer.name,
        homePlayerScore: game.homePlayerScore,
        awayPlayerScore: game.awayPlayerScore
      }))
    ].sort((a, b) => a.id - b.id);

    this.computeHighestWinViewModel();

    this.titlesForm = this.fb.group({
      titles: this.playerDetailData.titles
    });
  }

  computeHighestWinViewModel() {
    if (this.playerDetailData.highestWin) {
      const isHomePlayerInHighestWin =
        this.playerDetailData.highestWin.homePlayerScore >
        this.playerDetailData.highestWin.awayPlayerScore;
      const highestWinHomePlayer = this.playerDetailData.highestWin.homePlayer;
      const highestWinAwayPlayer = this.playerDetailData.highestWin.awayPlayer;
      this.highestWinViewModel = {
        opposingPlayer: isHomePlayerInHighestWin
          ? highestWinAwayPlayer.name
          : highestWinHomePlayer.name,
        opposingPlayerScore: isHomePlayerInHighestWin
          ? this.playerDetailData.highestWin.awayPlayerScore
          : this.playerDetailData.highestWin.homePlayerScore,
        thisPlayerScore: isHomePlayerInHighestWin
          ? this.playerDetailData.highestWin.homePlayerScore
          : this.playerDetailData.highestWin.awayPlayerScore
      };
    }
  }

  submitMethod(values: { titles: number }) {
    return this.playerService.updateTitles({ ...values, id: this.playerDetailData.id });
  }

  onFormSuccessfullSubmitted(updatedPlayer: { titles: number }) {
    this.playerDetailData.titles = updatedPlayer.titles;
  }
}
