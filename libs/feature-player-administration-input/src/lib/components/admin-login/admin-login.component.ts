import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'molbitz-tournaments-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent {
  loginForm: FormGroup;
  submitFunction = this.submitMethod.bind(this);

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly router: Router,
    private readonly fb: FormBuilder
  ) {
    this.loginForm = this.fb.group({
      username: '',
      password: ''
    });
  }

  submitMethod(values: { username: string; password: string }) {
    return this.authenticationService.login(values);
  }

  onFormSuccessfullSubmitted() {
    this.router.navigate(['/admin/tournaments']);
  }
}
