import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IGame, IPlayer, IRound } from '@molbitz-tournaments/util-common';
import { IAdditionalInformation } from 'libs/util-common/src/lib/types/additional-information.type';
import { throwError } from 'rxjs';
import { GameService } from '../../services/game.service';
import { GamesTableDataSource, GameSubmitModel } from '../../types/types';

type FormSubmittedGame = Omit<GameSubmitModel, 'round' | 'additionalInformation'> & {
  hasAdditionalInformation: boolean;
  extraTimeHomePlayerScore: number | null;
  extraTimeAwayPlayerScore: number | null;
  penaltyShootoutHomePlayerScore: number | null;
  penaltyShootoutAwayPlayerScore: number | null;
};

@Component({
  selector: 'molbitz-tournaments-round-detail',
  templateUrl: './round-detail.component.html',
  styleUrls: ['./round-detail.component.scss']
})
export class RoundDetailComponent {
  roundDetailData: IRound;
  gameForm: FormGroup;
  playerData: IPlayer[];
  gamesTableDataSource: GamesTableDataSource[];
  submitFunction = this.submitMethod.bind(this);

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly gameService: GameService
  ) {
    this.roundDetailData = this.route.snapshot.data.roundDetailData;
    this.playerData = this.route.snapshot.data.playersData;
    this.gamesTableDataSource = this.calculateTableDataSource(
      this.roundDetailData.games || []
    ).sort((a, b) => a.id - b.id);
    this.gameForm = this.fb.group({
      homePlayer: '',
      awayPlayer: '',
      homePlayerScore: 0,
      awayPlayerScore: 0,
      hasAdditionalInformation: false,
      hasPenaltyShootout: false,
      extraTimeHomePlayerScore: null,
      extraTimeAwayPlayerScore: null,
      penaltyShootoutHomePlayerScore: null,
      penaltyShootoutAwayPlayerScore: null
    });
  }

  submitMethod(submittedGame: FormSubmittedGame) {
    if (typeof submittedGame.homePlayer === 'string') {
      try {
        submittedGame.homePlayer = this.getPlayerByName(submittedGame.homePlayer);
      } catch (error) {
        return throwError(new Error('This Player does not exist'));
      }
    }
    if (typeof submittedGame.awayPlayer === 'string') {
      try {
        submittedGame.awayPlayer = this.getPlayerByName(submittedGame.awayPlayer);
      } catch (error) {
        return throwError(new Error('This Player does not exist'));
      }
    }
    const additionalInformation: Omit<
      IAdditionalInformation,
      'id'
    > | null = submittedGame.hasAdditionalInformation
      ? {
          extraTimeHomePlayerScore: submittedGame.extraTimeHomePlayerScore!,
          extraTimeAwayPlayerScore: submittedGame.extraTimeAwayPlayerScore!,
          penaltyShootoutHomePlayerScore: submittedGame.penaltyShootoutHomePlayerScore!,
          penaltyShootoutAwayPlayerScore: submittedGame.penaltyShootoutAwayPlayerScore!
        }
      : null;
    const gameSubmitModel: GameSubmitModel = {
      ...submittedGame,
      round: {
        id: this.roundDetailData.id
      },
      additionalInformation: submittedGame.hasAdditionalInformation
        ? additionalInformation
        : null
    };
    return this.gameService.createGame(gameSubmitModel);
  }

  onFormSuccessfullSubmitted(createdGame: IGame) {
    this.gamesTableDataSource = [
      ...this.gamesTableDataSource,
      {
        id: createdGame.id,
        homePlayerName: createdGame.homePlayer.name,
        awayPlayerName: createdGame.awayPlayer.name,
        homePlayerScore: createdGame.homePlayerScore,
        awayPlayerScore: createdGame.awayPlayerScore
      }
    ];
  }

  deleteGame(gameId: number) {
    this.gameService.deleteGame(gameId).subscribe((isDeleted) => {
      if (isDeleted) {
        this.roundDetailData.games = this.roundDetailData.games?.filter(
          (game) => game.id !== gameId
        );
        this.gamesTableDataSource = this.calculateTableDataSource(
          this.roundDetailData.games || []
        );
      }
    });
  }

  private getPlayerByName(name: string): IPlayer {
    const player = this.playerData.find((player) => player.name === name);
    if (player) {
      return player;
    } else {
      throw new Error(`Player with name ${name} is not defined`);
    }
  }

  private calculateTableDataSource(roundDetailGames: IGame[]) {
    return roundDetailGames.map((game) => ({
      id: game.id,
      homePlayerName: game.homePlayer.name,
      awayPlayerName: game.awayPlayer.name,
      homePlayerScore: game.homePlayerScore,
      awayPlayerScore: game.awayPlayerScore
    }));
  }
}
