import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IRound, ITournament } from '@molbitz-tournaments/util-common';
import { RoundService } from '../../services/round.service';

@Component({
  selector: 'molbitz-tournaments-tournament-detail',
  templateUrl: './tournament-detail.component.html',
  styleUrls: ['./tournament-detail.component.scss']
})
export class TournamentDetailComponent {
  tournamentDetailData: ITournament;

  roundForm: FormGroup;
  submitFunction = this.submitMethod.bind(this);

  roundListDataSource: ReadonlyArray<IRound>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly roundService: RoundService
  ) {
    this.roundForm = this.fb.group({
      title: ''
    });
    this.tournamentDetailData = this.route.snapshot.data.tournamentsData;

    this.roundListDataSource =
      this.tournamentDetailData.rounds?.sort((a, b) => a.id - b.id) || [];
  }

  submitMethod(values: { title: string }) {
    if (this.tournamentDetailData.id) {
      const body = {
        ...values,
        tournament: {
          id: this.tournamentDetailData.id
        }
      };
      return this.roundService.createRound(body);
    }
    throw new Error('tournament id not defined');
  }

  onFormSuccessfullSubmitted(createdRound: IRound) {
    this.roundListDataSource = [
      ...(this.tournamentDetailData.rounds || []),
      createdRound
    ];
  }
}
