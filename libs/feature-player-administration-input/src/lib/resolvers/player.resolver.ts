import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { IPlayer } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerResolver implements Resolve<IPlayer[]> {
  constructor(private readonly http: HttpClient) {}
  resolve(): Observable<IPlayer[]> {
    return this.http.get<IPlayer[]>(`players`);
  }
}
