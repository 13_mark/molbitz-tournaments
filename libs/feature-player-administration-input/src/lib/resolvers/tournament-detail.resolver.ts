import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { ITournament } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TournamentDetailResolver implements Resolve<ITournament> {
  constructor(private readonly http: HttpClient, private readonly router: Router) {}
  resolve(route: ActivatedRouteSnapshot): Observable<ITournament> {
    return this.http.get<ITournament>(`tournament/${route.paramMap.get('id')}`).pipe(
      catchError((error) => {
        console.error('tournament resolver error', error);
        this.router.navigate(['input/tournaments']);
        return EMPTY;
      })
    );
  }
}
