import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { IPlayer } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerDetailResolver implements Resolve<IPlayer> {
  constructor(private readonly http: HttpClient) {}
  resolve(route: ActivatedRouteSnapshot): Observable<IPlayer> {
    return this.http.get<IPlayer>(`player/${route.paramMap.get('id')}`);
  }
}
