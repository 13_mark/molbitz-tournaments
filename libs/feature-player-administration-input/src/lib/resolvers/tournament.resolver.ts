import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { ITournament } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TournamentResolver implements Resolve<ITournament[]> {
  constructor(private readonly http: HttpClient) {}

  resolve(): Observable<ITournament[]> {
    return this.http.get<ITournament[]>(`tournaments`);
  }
}
