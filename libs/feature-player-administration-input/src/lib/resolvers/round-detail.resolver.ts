import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { IRound } from '@molbitz-tournaments/util-common';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { RoundService } from '../services/round.service';

@Injectable({
  providedIn: 'root'
})
export class RoundDetailResolver implements Resolve<IRound | undefined> {
  constructor(private readonly roundService: RoundService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<IRound | undefined> {
    const tournamentId = Number(route.paramMap.get('tournamentId'));
    const roundId = Number(route.paramMap.get('roundId'));
    return tournamentId && roundId
      ? this.roundService.getRound(roundId, tournamentId)
      : of(undefined);
  }
}
