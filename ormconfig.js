const databaseHost = process.argv[2];
const databasePort = process.argv[3];
const databaseUser = process.argv[4];
const databasePassword = process.argv[5];

module.exports = {
  type: 'postgres',
  host: databaseHost ? databaseHost : 'localhost',
  port: databasePort ? databasePort : 5000,
  username: databaseUser ? databaseUser : 'postgres',
  password: databasePassword ? databasePassword : 'password',
  database: 'molbitz',
  synchronize: true,
  logging: false,
  entities: ['apps/player-administration-backend/src/entity/**/*.ts'],
  migrations: ['apps/player-administration-backend/src/migration/**/*.ts'],
  subscribers: ['apps/player-administration-backend/src/subscriber/**/*.ts'],
  cli: {
    entitiesDir: 'apps/player-administration-backend/src/entity',
    migrationsDir: 'apps/player-administration-backend/src/migration',
    subscribersDir: 'apps/player-administration-backend/src/subscriber'
  }
};
