FROM node:12.7-alpine AS build

ENV DB_SERVER_NAME=
ENV DB_SERVER_PORT=
ENV DB_USERNAME=
ENV DB_USER_PASSWORD=
ENV ADMIN_USERNAME=
ENV ADMIN_PASSWORD=

WORKDIR /usr/backend
COPY ./package.json /usr/backend/

RUN npm install
COPY ./ /usr/backend

EXPOSE 3000

CMD ["sh", "-c", "npm run start:backend:player-administration $DB_SERVER_NAME $DB_SERVER_PORT $DB_USERNAME $DB_USER_PASSWORD $ADMIN_USERNAME $ADMIN_PASSWORD"]