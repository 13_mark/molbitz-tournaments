import 'reflect-metadata';

import * as bodyParser from 'body-parser';

import { Request, Response } from 'express';
import { authenticateJWT, skipAuthentication } from './utils/authentication';

import { Routes } from './routes';
import cors from 'cors';
import { createConnection } from 'typeorm';
import express from 'express';

createConnection()
  .then(async () => {
    const app = express();

    app.use(bodyParser.json());

    // this should be removed in production
    app.use(cors());

    Routes.forEach((route) => {
      const authenticationMiddleware = route.skipAuthentication
        ? skipAuthentication
        : authenticateJWT;
      app[route.method](
        route.route,
        authenticationMiddleware,
        (req: Request, res: Response, next: Function) => {
          const controller = new route.controller();
          const result = controller[route.action](req, res, next);
          if (result instanceof Promise) {
            result
              .then((response) => {
                if (response) {
                  res.send(response);
                } else {
                  res.status(400).send('response was undefined');
                }
              })
              .catch((err: { message: string }) => {
                res.status(400).send(err.message);
              });
          } else if (result !== null && result !== undefined) {
            res.send(result);
          }
        }
      );
    });

    app.listen(3001);
    console.log(
      'Express server has started on port 3001. Open http://localhost:3001/users to see results'
    );
  })
  .catch((error) => console.log(error));
