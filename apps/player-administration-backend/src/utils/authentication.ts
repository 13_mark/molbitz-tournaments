import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';

export function authenticateJWT(req: Request, res: Response, next: NextFunction) {
  const authHeader = req.headers.authorization;
  if (!req.path.includes('/authenticate')) {
    if (authHeader) {
      const token = authHeader.split(' ')[1];
      jwt.verify(token, accessTokenSecret, (err): Response | void => {
        if (err) {
          return res.status(403).send('token is not valid');
        }
        next();
      });
    } else {
      res.status(401).send('user is not authorized');
    }
  } else {
    next();
  }
}

export function skipAuthentication(
  _req: Request,
  _res: Response,
  next: NextFunction
): void {
  next();
}

export const accessTokenSecret = 'RRm5BbpLjxeEGXRySPAYDE89bs38makcKG';
