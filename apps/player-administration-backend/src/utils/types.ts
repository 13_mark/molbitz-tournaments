export type GameResult = 'homeWin' | 'awayWin' | 'draw';
