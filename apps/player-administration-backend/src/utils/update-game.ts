import { Game } from '../entity/game';
import { GameResult } from './types';
import { Player } from '../entity/player';
import { getRepository } from 'typeorm';

function isNewHighestWin(player: Player, goalDifference: number): boolean {
  if (!player.highestWin) {
    return true;
  }
  const highestWinGoalDifference =
    player.highestWin.homePlayerScore > player.highestWin.awayPlayerScore
      ? player.highestWin.homePlayerScore - player.highestWin.awayPlayerScore
      : player.highestWin.awayPlayerScore - player.highestWin.homePlayerScore;
  if (highestWinGoalDifference < goalDifference) {
    return true;
  }

  return false;
}

function updatePlayersGoals(game: Game, homePlayer: Player, awayPlayer: Player) {
  homePlayer.goalsScored = homePlayer.goalsScored + game.homePlayerScore;
  homePlayer.goalsConceded = homePlayer.goalsConceded + game.awayPlayerScore;

  awayPlayer.goalsScored = awayPlayer.goalsScored + game.awayPlayerScore;
  awayPlayer.goalsConceded = awayPlayer.goalsConceded + game.homePlayerScore;
}

function updatePlayersOnHomeWin(game: Game, homePlayer: Player, awayPlayer: Player) {
  homePlayer.points = homePlayer.points + 3;
  homePlayer.wins = homePlayer.wins + 1;
  awayPlayer.loses = awayPlayer.loses + 1;
  if (isNewHighestWin(homePlayer, game.homePlayerScore - game.awayPlayerScore)) {
    homePlayer.highestWin = game;
  }
}

function updatePlayersOnAwayWin(game: Game, homePlayer: Player, awayPlayer: Player) {
  awayPlayer.points = awayPlayer.points + 3;
  awayPlayer.wins = awayPlayer.wins + 1;
  homePlayer.loses = homePlayer.loses + 1;
  if (isNewHighestWin(awayPlayer, game.awayPlayerScore - game.homePlayerScore)) {
    awayPlayer.highestWin = game;
  }
}

function updatePlayersOnDraw(homePlayer: Player, awayPlayer: Player) {
  awayPlayer.points = awayPlayer.points + 1;
  awayPlayer.draws = awayPlayer.draws + 1;
  homePlayer.points = homePlayer.points + 1;
  homePlayer.draws = homePlayer.draws + 1;
}

export async function updatePlayersOnGame(game: Game, gameResult: GameResult) {
  const playerRepository = getRepository(Player);

  const homePlayer = await playerRepository.findOne(game.homePlayer.id, {
    relations: ['highestWin']
  });
  const awayPlayer = await playerRepository.findOne(game.awayPlayer.id, {
    relations: ['highestWin']
  });

  if (homePlayer && awayPlayer) {
    updatePlayersGoals(game, homePlayer, awayPlayer);

    switch (gameResult) {
      case 'homeWin':
        updatePlayersOnHomeWin(game, homePlayer, awayPlayer);
        break;
      case 'awayWin':
        updatePlayersOnAwayWin(game, homePlayer, awayPlayer);
        break;
      case 'draw':
        updatePlayersOnDraw(homePlayer, awayPlayer);
        break;
      default:
        break;
    }

    await playerRepository.save(homePlayer);
    await playerRepository.save(awayPlayer);
  } else {
    throw new Error('One of the Players was not found');
  }
}
