import { getRepository } from 'typeorm';
import { Game } from '../entity/game';
import { Player } from '../entity/player';
import { GameResult } from './types';
import { cloneDeep } from 'lodash';
import { updateHighestWinsUpdateFunctions } from '../functions/highest-win-update-function';

interface GamePlayerOutputModel {
  homePlayer: Player;
  awayPlayer: Player;
}

type GameUpdateFunctions = {
  [key in GameResult]: (homePlayer: Player, awayPlayer: Player) => GamePlayerOutputModel;
};

const updatePointsUpdateFunctions: GameUpdateFunctions = {
  homeWin: (homePlayer, awayPlayer) => {
    return {
      homePlayer: {
        ...homePlayer,
        points: homePlayer.points - 3
      },
      awayPlayer
    };
  },
  draw: (homePlayer, awayPlayer) => {
    return {
      homePlayer: {
        ...homePlayer,
        points: homePlayer.points - 1
      },
      awayPlayer: {
        ...awayPlayer,
        points: awayPlayer.points - 1
      }
    };
  },
  awayWin: (homePlayer, awayPlayer) => {
    return {
      homePlayer,
      awayPlayer: {
        ...awayPlayer,
        points: awayPlayer.points - 3
      }
    };
  }
};

function updateGoals(
  homePlayer: Player,
  awayPlayer: Player,
  gameToRemove: Game
): GamePlayerOutputModel {
  return {
    homePlayer: {
      ...homePlayer,
      goalsScored: homePlayer.goalsScored - gameToRemove.homePlayerScore,
      goalsConceded: homePlayer.goalsConceded - gameToRemove.awayPlayerScore
    },
    awayPlayer: {
      ...awayPlayer,
      goalsScored: awayPlayer.goalsScored - gameToRemove.awayPlayerScore,
      goalsConceded: awayPlayer.goalsConceded - gameToRemove.homePlayerScore
    }
  };
}

const updateWinsAndLossesUpdateFunctions: GameUpdateFunctions = {
  homeWin: (homePlayer, awayPlayer) => {
    return {
      homePlayer: {
        ...homePlayer,
        wins: homePlayer.wins - 1
      },
      awayPlayer: {
        ...awayPlayer,
        loses: awayPlayer.loses - 1
      }
    };
  },
  draw: (homePlayer, awayPlayer) => {
    return {
      homePlayer: {
        ...homePlayer,
        draws: homePlayer.draws - 1
      },
      awayPlayer: {
        ...awayPlayer,
        draws: awayPlayer.draws - 1
      }
    };
  },
  awayWin: (homePlayer, awayPlayer) => {
    return {
      homePlayer: {
        ...homePlayer,
        loses: homePlayer.loses - 1
      },
      awayPlayer: {
        ...awayPlayer,
        wins: awayPlayer.wins - 1
      }
    };
  }
};

function updatePlayerGames(
  homePlayer: Player,
  awayPlayer: Player,
  gameToDelete: Game
): GamePlayerOutputModel {
  return {
    homePlayer: {
      ...homePlayer,
      homeGames: homePlayer.homeGames.filter((game) => gameToDelete.id !== game.id)
    },
    awayPlayer: {
      ...awayPlayer,
      awayGames: awayPlayer.awayGames.filter((game) => gameToDelete.id !== game.id)
    }
  };
}

export async function updatePlayersOnGameDelete(
  gameToRemove: Game,
  gameResult: GameResult
) {
  const playerRepository = getRepository(Player);
  const databaseHomePlayer = cloneDeep(
    await playerRepository.findOne(gameToRemove.homePlayer.id, {
      relations: ['highestWin', 'homeGames', 'awayGames']
    })
  );
  const databaseAwayPlayer = cloneDeep(
    await playerRepository.findOne(gameToRemove.awayPlayer.id, {
      relations: ['highestWin', 'homeGames', 'awayGames']
    })
  );

  if (databaseHomePlayer && databaseAwayPlayer) {
    const playersWithUpdatedPoints = updatePointsUpdateFunctions[gameResult](
      databaseHomePlayer,
      databaseAwayPlayer
    );
    const playersWithUpdatedGoals = updateGoals(
      playersWithUpdatedPoints.homePlayer,
      playersWithUpdatedPoints.awayPlayer,
      gameToRemove
    );
    const playersWithUpdatedWinsAndLosses = updateWinsAndLossesUpdateFunctions[
      gameResult
    ](playersWithUpdatedGoals.homePlayer, playersWithUpdatedGoals.awayPlayer);

    const playersWithUpdatedHighestWin =
      gameResult !== 'draw'
        ? updateHighestWinsUpdateFunctions(gameToRemove)[gameResult](
            playersWithUpdatedWinsAndLosses.homePlayer,
            playersWithUpdatedWinsAndLosses.awayPlayer
          )
        : playersWithUpdatedWinsAndLosses;

    const playersWithUpdatedGames = updatePlayerGames(
      playersWithUpdatedHighestWin.homePlayer,
      playersWithUpdatedHighestWin.awayPlayer,
      gameToRemove
    );

    return playersWithUpdatedGames;
  } else {
    throw new Error('One of the Players was not found');
  }
}
