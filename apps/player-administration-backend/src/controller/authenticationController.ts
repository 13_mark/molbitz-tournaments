import * as jwt from 'jsonwebtoken';

import { Request } from 'express';
import { accessTokenSecret } from '../utils/authentication';

export class AuthentiactionController {
  private readonly credentials = {
    username: process.argv[6] ? process.argv[6] : 'admin',
    password: process.argv[7] ? process.argv[7] : 'admin'
  };

  login(request: Request) {
    const { username, password } = request.body;
    if (
      username === this.credentials.username &&
      password === this.credentials.password
    ) {
      const accessToken = jwt.sign({ username }, accessTokenSecret);
      return Promise.resolve({ token: accessToken });
    } else {
      return Promise.reject('Login failed');
    }
  }
}
