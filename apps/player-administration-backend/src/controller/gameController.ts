import { Game } from '../entity/game';
import { GameResult } from '../utils/types';
import { Request } from 'express';
import { getRepository } from 'typeorm';
import { updatePlayersOnGame } from '../utils/update-game';
import { updatePlayersOnGameDelete } from '../utils/update-players-on-game-delete';
import { Round } from '../entity/round';
import { Player } from '../entity/player';
import { cloneDeep } from 'lodash';
import fetch from 'node-fetch';

function getGameResult(game: Game): GameResult {
  return game.homePlayerScore > game.awayPlayerScore
    ? 'homeWin'
    : game.homePlayerScore < game.awayPlayerScore
    ? 'awayWin'
    : 'draw';
}

export class GameController {
  private gameRepository = getRepository(Game);
  private playerRepository = getRepository(Player);
  private roundRepository = getRepository(Round);

  async all() {
    return this.gameRepository.find({
      relations: ['homePlayer', 'awayPlayer', 'round', 'additionalInformation']
    });
  }

  async one(request: Request) {
    return this.gameRepository.findOne(request.params.id);
  }

  async save(request: Request) {
    const gameResult = getGameResult(request.body);
    const game = await this.gameRepository.save(request.body);
    await updatePlayersOnGame(game, gameResult);

    await sendPushNotification(game);

    return game;
  }

  async remove(request: Request) {
    const gameToRemove = await this.gameRepository.findOne(request.params.id, {
      relations: ['homePlayer', 'awayPlayer', 'round']
    });

    if (gameToRemove) {
      const gameResult = getGameResult(gameToRemove);
      // TODO: it seems like a code smell that I have to await this function
      //       a better approach would be to insert all Parameters as input
      const updatedPlayer = await updatePlayersOnGameDelete(gameToRemove, gameResult);

      const updatedRound = await updateRoundOnGameDelete(gameToRemove);

      await this.playerRepository.save(updatedPlayer.homePlayer);
      await this.playerRepository.save(updatedPlayer.awayPlayer);
      await this.roundRepository.save(updatedRound);
      await this.gameRepository.remove(gameToRemove);

      return true;
    }

    return false;
  }
}

async function updateRoundOnGameDelete(gameToDelete: Game): Promise<Round> {
  const roundRepo = getRepository(Round);
  const databaseRound = cloneDeep(
    await roundRepo.findOne(gameToDelete.round.id, {
      relations: ['games']
    })
  );
  if (databaseRound) {
    return {
      ...databaseRound,
      games: databaseRound.games.filter((game) => game.id !== gameToDelete.id)
    };
  }

  throw new Error('Unable to load round');
}

async function sendPushNotification(game: Game) {
  const {
    homePlayer,
    awayPlayer,
    homePlayerScore: homeScore,
    awayPlayerScore: awayScore
  } = game;

  return await fetch('https://notifications.molbitzer-turniere.de/notify', {
    method: 'POST',
    body: JSON.stringify({
      home: homePlayer.name,
      away: awayPlayer.name,
      homeScore,
      awayScore
    }),
    headers: {
      'content-type': 'application/json'
    }
  });
}
