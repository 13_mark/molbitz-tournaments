import { Player } from '../entity/player';
import { Request } from 'express';
import { getRepository } from 'typeorm';

export class PlayerController {
  private playerRepository = getRepository(Player);

  async all() {
    return this.playerRepository.find({
      relations: [
        'homeGames',
        'awayGames',
        'highestWin',
        'highestWin.awayPlayer',
        'highestWin.homePlayer',
        'homeGames.round',
        'homeGames.round.tournament',
        'awayGames.round',
        'awayGames.round.tournament'
      ]
    });
  }

  async one(request: Request) {
    return this.playerRepository.findOne(request.params.id, {
      relations: [
        'homeGames',
        'awayGames',
        'highestWin',
        'homeGames.homePlayer',
        'homeGames.awayPlayer',
        'awayGames.homePlayer',
        'awayGames.awayPlayer',
        'highestWin.homePlayer',
        'highestWin.awayPlayer',
        'homeGames.round',
        'homeGames.round.tournament',
        'awayGames.round',
        'awayGames.round.tournament'
      ]
    });
  }

  async save(request: Request) {
    return this.playerRepository.save(request.body);
  }

  async update(request: Request) {
    const player = await this.playerRepository.findOne(request.params.id, {
      relations: ['highestWin', 'highestWin.homePlayer', 'highestWin.awayPlayer']
    });
    const updateData = { ...player, ...request.body };

    return this.playerRepository.save(updateData);
  }

  async remove(request: Request) {
    const userToRemove = await this.playerRepository.findOne(request.params.id);
    if (userToRemove) {
      await this.playerRepository.remove(userToRemove);
    }
  }
}
