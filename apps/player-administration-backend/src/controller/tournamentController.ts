import { getRepository } from 'typeorm';
import { Request } from 'express';
import { Tournament } from '../entity/tournament';

export class TournamentController {
  private tournamentRepository = getRepository(Tournament);

  async all() {
    return this.tournamentRepository.find({
      relations: [
        'rounds',
        'rounds.games',
        'rounds.games.homePlayer',
        'rounds.games.awayPlayer',
        'rounds.games.additionalInformation'
      ]
    });
  }

  async one(request: Request) {
    return this.tournamentRepository.findOne(request.params.id, {
      relations: ['rounds']
    });
  }

  async save(request: Request) {
    return this.tournamentRepository.save(request.body);
  }

  async remove(request: Request) {
    const userToRemove = await this.tournamentRepository.findOne(request.params.id);
    if (userToRemove) {
      await this.tournamentRepository.remove(userToRemove);
    }
  }
}
