import { getRepository } from 'typeorm';
import { Request } from 'express';
import { Round } from '../entity/round';

export class RoundController {
  private roundRepository = getRepository(Round);

  async all() {
    return this.roundRepository.find();
  }

  async one(request: Request) {
    return this.roundRepository.findOne(request.params.id, {
      relations: ['games', 'games.homePlayer', 'games.awayPlayer']
    });
  }

  async save(request: Request) {
    return this.roundRepository.save(request.body);
  }

  async remove(request: Request) {
    const userToRemove = await this.roundRepository.findOne(request.params.id);
    if (userToRemove) {
      await this.roundRepository.remove(userToRemove);
    }
  }
}
