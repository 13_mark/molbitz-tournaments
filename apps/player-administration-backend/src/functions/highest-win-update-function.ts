import { Game } from '../entity/game';
import { Player } from '../entity/player';
import { computeHighestWin } from './compute-highest-win';

export function updateHighestWinsUpdateFunctions(
  gameToRemove: Game
): {
  homeWin: (
    homePlayer: Player,
    awayPlayer: Player
  ) => {
    homePlayer: Player;
    awayPlayer: Player;
  };
  awayWin: (
    homePlayer: Player,
    awayPlayer: Player
  ) => {
    homePlayer: Player;
    awayPlayer: Player;
  };
} {
  return {
    homeWin: (homePlayer, awayPlayer) => {
      const filteredGames = {
        homePlayer: {
          homeGames: homePlayer.homeGames.filter((game) => game.id !== gameToRemove.id),
          awayGames: homePlayer.awayGames.filter((game) => game.id !== gameToRemove.id)
        },
        awayPlayer: {
          homeGames: awayPlayer.homeGames.filter((game) => game.id !== gameToRemove.id),
          awayGames: awayPlayer.awayGames.filter((game) => game.id !== gameToRemove.id)
        }
      };

      const computedHighestWin = computeHighestWin(
        filteredGames.homePlayer.homeGames,
        filteredGames.homePlayer.awayGames
      );

      return {
        homePlayer: {
          ...homePlayer,
          ...filteredGames.homePlayer,
          highestWin: computedHighestWin
        },
        awayPlayer: {
          ...awayPlayer,
          ...filteredGames.awayPlayer
        }
      };
    },
    awayWin: (homePlayer, awayPlayer) => {
      const filteredGames = {
        homePlayer: {
          homeGames: homePlayer.homeGames.filter((game) => game.id !== gameToRemove.id),
          awayGames: homePlayer.awayGames.filter((game) => game.id !== gameToRemove.id)
        },
        awayPlayer: {
          homeGames: awayPlayer.homeGames.filter((game) => game.id !== gameToRemove.id),
          awayGames: awayPlayer.awayGames.filter((game) => game.id !== gameToRemove.id)
        }
      };

      const computedHighestWin = computeHighestWin(
        filteredGames.awayPlayer.homeGames,
        filteredGames.awayPlayer.awayGames
      );
      return {
        homePlayer: {
          ...homePlayer,
          ...filteredGames.homePlayer
        },
        awayPlayer: {
          ...awayPlayer,
          ...filteredGames.awayPlayer,
          highestWin: computedHighestWin
        }
      };
    }
  };
}
