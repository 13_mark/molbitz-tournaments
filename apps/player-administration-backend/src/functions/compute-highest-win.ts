import { Game } from '../entity/game';

export function computeHighestWin(homeGames: Game[], awayGames: Game[]): Game | null {
  const highestHomeWin = reduceHighestWinFromGames(homeGames, 'homeWin');
  const highestAwayWin = reduceHighestWinFromGames(awayGames, 'awayWin');

  if (highestHomeWin && !highestAwayWin) return highestHomeWin;
  if (!highestHomeWin && highestAwayWin) return highestAwayWin;

  if (highestHomeWin && highestAwayWin) {
    const homeWinGoalDifference =
      highestHomeWin.homePlayerScore - highestHomeWin.awayPlayerScore;
    const awayWinGoalDifference =
      highestAwayWin.awayPlayerScore - highestAwayWin.homePlayerScore;

    return homeWinGoalDifference >= awayWinGoalDifference
      ? highestHomeWin
      : highestAwayWin;
  }

  return null;
}

function reduceHighestWinFromGames(
  games: Game[],
  direction: 'homeWin' | 'awayWin'
): Game | undefined {
  if (direction === 'homeWin') {
    return games.reduce((acc: Game | undefined, curr: Game) => {
      if (acc) {
        if (
          curr.homePlayerScore - curr.awayPlayerScore >
          acc.homePlayerScore - acc.awayPlayerScore
        ) {
          return curr;
        } else {
          return acc;
        }
      } else {
        if (curr.homePlayerScore - curr.awayPlayerScore > 0) {
          return curr;
        } else {
          return undefined;
        }
      }
    }, undefined);
  } else {
    return games.reduce((acc: Game | undefined, curr: Game) => {
      if (acc) {
        if (
          curr.awayPlayerScore - curr.homePlayerScore >
          acc.awayPlayerScore - acc.homePlayerScore
        ) {
          return curr;
        } else {
          return acc;
        }
      } else {
        if (curr.awayPlayerScore - curr.homePlayerScore > 0) {
          return curr;
        } else {
          return undefined;
        }
      }
    }, undefined);
  }
}
