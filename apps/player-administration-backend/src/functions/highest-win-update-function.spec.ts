import { Game } from '../entity/game';
import { Player } from '../entity/player';
import { updateHighestWinsUpdateFunctions } from './highest-win-update-function';

interface GameRelevantInformation {
  id: number;
  homePlayerScore: number;
  awayPlayerScore: number;
}

interface PlayerRelevantInformation {
  homeGames: GameRelevantInformation[];
  awayGames: GameRelevantInformation[];
  highestWin: GameRelevantInformation | null;
}

interface TestCase {
  description: string;
  input: {
    gameToRemove: GameRelevantInformation;
    gameResult: 'homeWin' | 'awayWin';
    homePlayer: PlayerRelevantInformation;
    awayPlayer: PlayerRelevantInformation;
  };
  expected: {
    homePlayer: PlayerRelevantInformation;
    awayPlayer: PlayerRelevantInformation;
  };
}

const testCases: TestCase[] = [
  {
    description: 'updates highest win on home win',
    input: {
      gameToRemove: {
        id: 1,
        homePlayerScore: 5,
        awayPlayerScore: 0
      },
      gameResult: 'homeWin',
      homePlayer: {
        homeGames: [
          {
            id: 1,
            homePlayerScore: 5,
            awayPlayerScore: 0
          },
          {
            id: 2,
            homePlayerScore: 2,
            awayPlayerScore: 1
          }
        ],
        awayGames: [],
        highestWin: {
          id: 1,
          homePlayerScore: 5,
          awayPlayerScore: 0
        }
      },
      awayPlayer: {
        homeGames: [],
        awayGames: [
          {
            id: 1,
            homePlayerScore: 5,
            awayPlayerScore: 0
          }
        ],
        highestWin: null
      }
    },
    expected: {
      homePlayer: {
        homeGames: [
          {
            id: 2,
            homePlayerScore: 2,
            awayPlayerScore: 1
          }
        ],
        awayGames: [],
        highestWin: {
          id: 2,
          homePlayerScore: 2,
          awayPlayerScore: 1
        }
      },
      awayPlayer: {
        homeGames: [],
        awayGames: [],
        highestWin: null
      }
    }
  },
  {
    description: 'updates highest win on away win',
    input: {
      gameToRemove: {
        id: 1,
        homePlayerScore: 0,
        awayPlayerScore: 5
      },
      gameResult: 'awayWin',
      homePlayer: {
        homeGames: [
          {
            id: 1,
            homePlayerScore: 0,
            awayPlayerScore: 5
          }
        ],
        awayGames: [],
        highestWin: null
      },
      awayPlayer: {
        homeGames: [
          {
            id: 2,
            homePlayerScore: 2,
            awayPlayerScore: 0
          }
        ],
        awayGames: [
          {
            id: 1,
            homePlayerScore: 0,
            awayPlayerScore: 5
          }
        ],
        highestWin: {
          id: 1,
          homePlayerScore: 0,
          awayPlayerScore: 5
        }
      }
    },
    expected: {
      homePlayer: {
        homeGames: [],
        awayGames: [],
        highestWin: null
      },
      awayPlayer: {
        homeGames: [
          {
            id: 2,
            homePlayerScore: 2,
            awayPlayerScore: 0
          }
        ],
        awayGames: [],
        highestWin: {
          id: 2,
          homePlayerScore: 2,
          awayPlayerScore: 0
        }
      }
    }
  },
  {
    description: 'sets highest win to null when no highest win exists',
    input: {
      gameToRemove: {
        id: 1,
        homePlayerScore: 5,
        awayPlayerScore: 0
      },
      gameResult: 'homeWin',
      homePlayer: {
        homeGames: [
          {
            id: 1,
            homePlayerScore: 5,
            awayPlayerScore: 0
          }
        ],
        awayGames: [],
        highestWin: {
          id: 1,
          homePlayerScore: 5,
          awayPlayerScore: 0
        }
      },
      awayPlayer: {
        homeGames: [],
        awayGames: [
          {
            id: 1,
            homePlayerScore: 5,
            awayPlayerScore: 0
          }
        ],
        highestWin: null
      }
    },
    expected: {
      homePlayer: {
        homeGames: [],
        awayGames: [],
        highestWin: null
      },
      awayPlayer: {
        homeGames: [],
        awayGames: [],
        highestWin: null
      }
    }
  },
  {
    description: 'does not update highest win when highest win does not get deleted',
    input: {
      gameToRemove: {
        id: 1,
        homePlayerScore: 2,
        awayPlayerScore: 1
      },
      gameResult: 'homeWin',
      homePlayer: {
        homeGames: [
          {
            id: 1,
            homePlayerScore: 2,
            awayPlayerScore: 1
          },
          {
            id: 2,
            homePlayerScore: 5,
            awayPlayerScore: 0
          }
        ],
        awayGames: [],
        highestWin: {
          id: 2,
          homePlayerScore: 5,
          awayPlayerScore: 0
        }
      },
      awayPlayer: {
        homeGames: [
          {
            id: 3,
            homePlayerScore: 2,
            awayPlayerScore: 0
          }
        ],
        awayGames: [
          {
            id: 1,
            homePlayerScore: 2,
            awayPlayerScore: 1
          }
        ],
        highestWin: {
          id: 3,
          homePlayerScore: 2,
          awayPlayerScore: 0
        }
      }
    },
    expected: {
      homePlayer: {
        homeGames: [
          {
            id: 2,
            homePlayerScore: 5,
            awayPlayerScore: 0
          }
        ],
        awayGames: [],
        highestWin: {
          id: 2,
          homePlayerScore: 5,
          awayPlayerScore: 0
        }
      },
      awayPlayer: {
        homeGames: [
          {
            id: 3,
            homePlayerScore: 2,
            awayPlayerScore: 0
          }
        ],
        awayGames: [],
        highestWin: {
          id: 3,
          homePlayerScore: 2,
          awayPlayerScore: 0
        }
      }
    }
  }
];

const dummyGame = new Game();
const dummyPlayer = new Player();

describe('highest win update function', () => {
  for (const testCase of testCases) {
    it(testCase.description, () => {
      const preparedTestObject = computeTestCaseObject(
        testCase.input.gameToRemove,
        testCase.input.homePlayer,
        testCase.input.awayPlayer
      );
      const computedResult = updateHighestWinsUpdateFunctions(
        preparedTestObject.gameToRemove
      )[testCase.input.gameResult](
        preparedTestObject.homePlayer,
        preparedTestObject.awayPlayer
      );
      const expectedResult = {
        homePlayer: {
          ...dummyPlayer,
          ...testCase.expected.homePlayer,
          highestWin: testCase.expected.homePlayer.highestWin
            ? {
                ...testCase.expected.homePlayer.highestWin,
                additionalInformation: null
              }
            : null,
          homeGames: testCase.expected.homePlayer.homeGames.map((game) => ({
            ...dummyGame,
            ...game
          })),
          awayGames: testCase.expected.homePlayer.awayGames.map((game) => ({
            ...dummyGame,
            ...game
          }))
        },
        awayPlayer: {
          ...dummyPlayer,
          ...testCase.expected.awayPlayer,
          highestWin: testCase.expected.awayPlayer.highestWin
            ? {
                ...testCase.expected.awayPlayer.highestWin,
                additionalInformation: null
              }
            : null,
          homeGames: testCase.expected.awayPlayer.homeGames.map((game) => ({
            ...dummyGame,
            ...game
          })),
          awayGames: testCase.expected.awayPlayer.awayGames.map((game) => ({
            ...dummyGame,
            ...game
          }))
        }
      };
      expect(computedResult).toEqual(expectedResult);
    });
  }
});

function computeTestCaseObject(
  gameToRemoveTestCaseData: GameRelevantInformation,
  homePlayerDummy: PlayerRelevantInformation,
  awayPlayerDummy: PlayerRelevantInformation
): { gameToRemove: Game; homePlayer: Player; awayPlayer: Player } {
  const gameToRemove: Game = {
    ...dummyGame,
    ...gameToRemoveTestCaseData
  };

  const homePlayer: Player = {
    ...dummyPlayer,
    homeGames: homePlayerDummy.homeGames.map((game) => ({ ...dummyGame, ...game })),
    awayGames: homePlayerDummy.awayGames.map((game) => ({ ...dummyGame, ...game })),
    highestWin: homePlayerDummy.highestWin
      ? {
          ...dummyGame,
          ...homePlayerDummy.highestWin
        }
      : null
  };

  const awayPlayer = {
    ...dummyPlayer,
    homeGames: awayPlayerDummy.homeGames.map((game) => ({ ...dummyGame, ...game })),
    awayGames: awayPlayerDummy.awayGames.map((game) => ({ ...dummyGame, ...game })),
    highestWin: awayPlayerDummy.highestWin
      ? {
          ...dummyGame,
          ...awayPlayerDummy.highestWin
        }
      : null
  };

  return { gameToRemove, homePlayer, awayPlayer };
}
