import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { ITournament } from '@molbitz-tournaments/util-common';
import { Round } from './round';

@Entity()
export class Tournament implements ITournament {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'date' })
  date!: Date;

  @OneToMany(() => Round, (round) => round.tournament)
  rounds!: Round[];
}
