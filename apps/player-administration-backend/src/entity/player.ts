import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';

import { Game } from './game';
import { IPlayer } from '@molbitz-tournaments/util-common';

@Entity()
export class Player implements IPlayer {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column({ nullable: true, default: 0 })
  points!: number;

  @Column({ nullable: true, default: 0 })
  goalsScored!: number;

  @Column({ nullable: true, default: 0 })
  goalsConceded!: number;

  @Column({ nullable: true, default: 0 })
  wins!: number;

  @Column({ nullable: true, default: 0 })
  draws!: number;

  @Column({ nullable: true, default: 0 })
  loses!: number;

  @Column({ nullable: true, default: 0 })
  titles!: number;

  @OneToMany(() => Game, (game) => game.homePlayer)
  homeGames!: Game[];

  @OneToMany(() => Game, (game) => game.awayPlayer)
  awayGames!: Game[];

  @OneToOne(() => Game)
  @JoinColumn()
  highestWin: Game | null = null;
}
