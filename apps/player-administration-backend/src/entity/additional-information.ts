import { IAdditionalInformation } from 'libs/util-common/src/lib/types/additional-information.type';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class AdditionalInformation implements IAdditionalInformation {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  extraTimeHomePlayerScore!: number;

  @Column()
  extraTimeAwayPlayerScore!: number;

  @Column({ nullable: true })
  penaltyShootoutHomePlayerScore?: number;

  @Column({ nullable: true })
  penaltyShootoutAwayPlayerScore?: number;
}
