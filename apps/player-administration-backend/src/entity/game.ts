import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';

import { AdditionalInformation } from './additional-information';
import { IGame } from '@molbitz-tournaments/util-common';
import { Player } from './player';
import { Round } from './round';

@Entity()
export class Game implements IGame {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  homePlayerScore!: number;

  @Column()
  awayPlayerScore!: number;

  @ManyToOne(() => Round, (round) => round.games)
  round!: Round;

  @ManyToOne(() => Player, (player) => player.homeGames)
  homePlayer!: Player;

  @ManyToOne(() => Player, (player) => player.awayGames)
  awayPlayer!: Player;

  @OneToOne(() => AdditionalInformation, { cascade: true })
  @JoinColumn()
  additionalInformation: AdditionalInformation | null = null;
}
