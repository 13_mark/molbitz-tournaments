import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Game } from './game';
import { IRound } from '@molbitz-tournaments/util-common';
import { Tournament } from './tournament';

@Entity()
export class Round implements IRound {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  title!: string;

  @ManyToOne(() => Tournament, (tournament) => tournament.rounds)
  tournament!: Tournament;

  @OneToMany(() => Game, (game) => game.round)
  games!: Game[];
}
