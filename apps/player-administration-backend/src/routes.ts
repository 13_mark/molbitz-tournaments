import { AuthentiactionController } from './controller/authenticationController';
import { GameController } from './controller/gameController';
import { PlayerController } from './controller/playerController';
import { RoundController } from './controller/roundController';
import { TournamentController } from './controller/tournamentController';
interface ExpressRoute<T> {
  method: 'get' | 'post' | 'put' | 'delete';
  route: string;
  controller: any;
  action: keyof T;
  skipAuthentication?: true;
}
const tournamentRoutes: ExpressRoute<TournamentController>[] = [
  {
    method: 'get',
    route: '/tournaments',
    controller: TournamentController,
    action: 'all',
    skipAuthentication: true
  },
  {
    method: 'get',
    route: '/tournament/:id',
    controller: TournamentController,
    action: 'one'
  },
  {
    method: 'post',
    route: '/tournament',
    controller: TournamentController,
    action: 'save'
  }
];
const roundRoutes: ExpressRoute<RoundController>[] = [
  {
    method: 'get',
    route: '/rounds',
    controller: RoundController,
    action: 'all'
  },
  {
    method: 'get',
    route: '/round/:id',
    controller: RoundController,
    action: 'one'
  },
  {
    method: 'post',
    route: '/round',
    controller: RoundController,
    action: 'save'
  }
];
const gameRoutes: ExpressRoute<GameController>[] = [
  {
    method: 'get',
    route: '/games',
    controller: GameController,
    action: 'all'
  },
  {
    method: 'post',
    route: '/game',
    controller: GameController,
    action: 'save'
  },
  {
    method: 'delete',
    route: '/game/:id',
    controller: GameController,
    action: 'remove'
  }
];
const playerRoutes: ExpressRoute<PlayerController>[] = [
  {
    method: 'get',
    route: '/players',
    controller: PlayerController,
    action: 'all',
    skipAuthentication: true
  },
  {
    method: 'get',
    route: '/player/:id',
    controller: PlayerController,
    action: 'one'
  },
  {
    method: 'post',
    route: '/player',
    controller: PlayerController,
    action: 'save'
  },
  {
    method: 'put',
    route: '/player/:id',
    controller: PlayerController,
    action: 'update'
  }
];

const authenticationRoutes: ExpressRoute<AuthentiactionController>[] = [
  {
    method: 'post',
    route: '/authenticate',
    controller: AuthentiactionController,
    action: 'login',
    skipAuthentication: true
  }
];

export const Routes = [
  ...playerRoutes,
  ...roundRoutes,
  ...tournamentRoutes,
  ...gameRoutes,
  ...authenticationRoutes
];
