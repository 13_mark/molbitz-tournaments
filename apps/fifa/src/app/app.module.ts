import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UiCommonModule } from '@molbitz-tournaments/ui-common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseUrlInterceptor } from '@molbitz-tournaments/util-common';
import { environment } from '../environments/environment';

const APP_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  {
    path: 'home',
    loadChildren: () =>
      import('@molbitz-tournaments/feature-home').then((m) => m.FeatureHomeModule)
  },
  {
    path: 'table',
    loadChildren: () =>
      import('@molbitz-tournaments/feature-table').then((m) => m.FeatureTableModule)
  },
  {
    path: 'tournaments',
    loadChildren: () =>
      import('@molbitz-tournaments/feature-tournaments').then(
        (m) => m.FeatureTournamentsModule
      )
  },
  {
    path: 'credits',
    loadChildren: () =>
      import('@molbitz-tournaments/feature-credits').then((m) => m.FeatureCreditsModule)
  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    UiCommonModule,
    RouterModule.forRoot(APP_ROUTES),
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseUrlInterceptor,
      multi: true
    },
    {
      provide: 'API_BASE_URL',
      useValue: environment.baseUrl
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
