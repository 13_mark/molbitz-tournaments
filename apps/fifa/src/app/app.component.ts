import { Component } from '@angular/core';

@Component({
  selector: 'molbitz-tournaments-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {}
