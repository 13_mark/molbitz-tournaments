import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BaseUrlInterceptor } from '@molbitz-tournaments/util-common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PlayerAdministrationInterceptor } from '@molbitz-tournaments/feature-player-administration-input';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { environment } from '../environments/environment';

const APP_ROUTES: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@molbitz-tournaments/feature-player-administration-input').then(
        (m) => m.FeaturePlayerAdministrationInputModule
      )
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, HttpClientModule, RouterModule.forRoot(APP_ROUTES)],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: PlayerAdministrationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseUrlInterceptor,
      multi: true
    },
    {
      provide: 'API_BASE_URL',
      useValue: environment.baseUrl
    }
  ]
})
export class AppModule {}
