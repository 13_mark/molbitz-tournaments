import { Component } from '@angular/core';
import { ResolveEnd, ResolveStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'molbitz-tournaments-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loadingIndicator = false;
  private readonly subscriptions: Subscription[] = [];

  constructor(private readonly router: Router) {
    this.subscriptions.push(
      this.router.events.subscribe((routerEvent) => {
        if (routerEvent instanceof ResolveStart) {
          this.loadingIndicator = true;
        }

        if (routerEvent instanceof ResolveEnd) {
          this.loadingIndicator = false;
        }
      })
    );
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
